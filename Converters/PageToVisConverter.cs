﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using ImageImport.MainWindows;

namespace ImageImport.Converters {

	/*
	 * Возвращает видимость страницы исходя из ее типа и переданного параметра
	 */
	class PageToVisConverter : IValueConverter {

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			var result = (Page)value;
			switch (result) {
				case Page.Progress:
					return (string)parameter == "progress" ? Visibility.Visible : Visibility.Collapsed;
				case Page.Message:
					return (string)parameter == "message" ? Visibility.Visible : Visibility.Collapsed;
				case Page.Undefined:
				default:
					return Visibility.Collapsed;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
