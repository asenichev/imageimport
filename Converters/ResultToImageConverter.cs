﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using CSUtil;

namespace ImageImport.Converters {

	/*
	 * Возвращает видимость исходя из значения Result и переданного параметра
	 */
	class ResultToImageConverter : IValueConverter {

		public object Convert(object value, Type targetType, object parameter,
			System.Globalization.CultureInfo culture) {
			
			var result = (Result)value;

			switch (result) {
				case Result.Undefined:
				case Result.Cancel:
					return Visibility.Hidden;
				case Result.OK:
					return (string)parameter == "ok" ? Visibility.Visible : Visibility.Hidden;
				case Result.Warning:
					return (string)parameter == "warning" ? Visibility.Visible : Visibility.Hidden;
				case Result.Error:
					return (string)parameter == "error" ? Visibility.Visible : Visibility.Hidden;
				default:
					throw new ApplicationException("Unknown result: " + result);
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
