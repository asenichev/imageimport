﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageImport.Models;

namespace ImageImport.Cycles {

	/*
	* Переименовывает файлы, которые имели расширение .jpg, но по факту
	* не имели необходимых свойств jpeg файла
	* Новые имена просто GUIDs
	*/
	class RenameOdd {

		IList<Photo> _photos;

		public RenameOdd(IEnumerable<Photo> photos) {
			_photos=photos.ToList();
			Rename();
		}

		private void Rename() {
			foreach (var photo in _photos) {
				photo.Name = Guid.NewGuid().ToString() + AppConstants.JpgExtension;
			}
		}

		public IEnumerable<Photo> RenamedPhotos {
			get {
				return _photos;
			}
		}
	}
}
