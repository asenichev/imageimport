﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ImageImport.Cycles {

	/*
	 * Производит в указанной папке и подпапках поиск всех файлов поддерживаемого формата.
	 * 
	 * Проверяет, что в указанной папке и подпапках нет файлов с расширениями других
	 * известных медиа форматов.
	 * 
	 * Подсчитывает количество всех файлов и папок в папке и подпапках
	 */
	public class SearchCycle {

		//количество файлов изображений (отдельное поле для свойства класса
		//поскольку это свойство часто используется программой)
		int _imageCount;

		//количество всех файлов и подпапок в директории
		int _allFilesCount;

		//количество неподдерживаемых медиа файлов
		int _mediaFilesCount;

		//найденные файлы изображений
		IEnumerable<FileInfo> _images;

		//найденные неподдерживаемые файлы
		IEnumerable<FileInfo> _unsupportedMediaFiles;


		public SearchCycle(DirectoryInfo sourceDir) {

			//считать все файлы и подпапки
			var allFiles = sourceDir.GetFileSystemInfos("*", SearchOption.AllDirectories);

			//записать количество всех файлов и подпапок
			_allFilesCount = allFiles.Length;

			//создать список поддерживаемых файлов
			SearchForSupportedMedia(allFiles);

			//создать список неподдерживаемых файлов
			SearchForUnsupportedMedia(allFiles);
		}

		//только изображения
		void SearchForSupportedMedia(FileSystemInfo[] files) {

			//искать среди файлов
			_images = files.Where(f => f is FileInfo).

				//если расширение файла совпадает с поддерживаемым
				Where(f => AppConstants.SupportedExtension.IsMatch(f.Name)).

				//преобразовать в объект FileInfo
				Select(f => (FileInfo)f);

			//записать количество файлов изображений
			_imageCount = _images.Count();
		}

		//другие медиа файлы
		void SearchForUnsupportedMedia(FileSystemInfo[] files) {

			//искать среди файлов
			_unsupportedMediaFiles = files.Where(f => f is FileInfo).

				//если расширение файла совпадает с одним из известных расширений медиа файлов
				Where(f => AppConstants.MediaFiles.IsMatch(f.Name)).

				//преобразовать в объект FileInfo
				Select(f => (FileInfo)f);

			//записать количество медиа файлов
			_mediaFilesCount = _unsupportedMediaFiles.Count();
		}

		public int AllFilesCount {
			get {
				return _allFilesCount;
			}
		}
		public int ImageFilesCount {
			get {
				return _imageCount;
			}
		}
		public int MediaFilesCount {
			get {
				return _mediaFilesCount;
			}
		}
		public IEnumerable<FileInfo> ImageFiles {
			get {
				return _images;
			}
		}
		public bool HasUnsupportedMedia {
			get {
				return _unsupportedMediaFiles.Count() > 0;
			}
		}

		/*
		 * список полных имен найденных неподдерживаемых медиа файлов
		 * каждое имя в отдельной строке
		 * после последней строки переноса нет
		 */
		public string UnsupportedMediaList {
			get {
				var list = _unsupportedMediaFiles.ToList();
				var b = new StringBuilder();
				for (int i = 0; i < list.Count - 1; i++) {
					b.AppendLine(list[i].FullName);
				}
				if (list.Count > 0) {
					b.Append(list[list.Count - 1].FullName);
				}
				return b.ToString();
			}
		}
	}
}
