﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CSUtil;
using ImageImport.Models;
using log4net;

namespace ImageImport.Cycles {

	/*
	 * Извлекает из объектов FileInfo все необходимые данные для импорта
	 * и оборачивает их в объект Photo.
	 * 
	 * Класс сначала создается, затем запускается отдельным асинхронным методом
	 */
	public class TransformCycle {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		//файлы изображений
		IEnumerable<FileInfo> _images;

		//результат выполнения цикла - объекты метаданных
		IList<Photo> _photos;

		//делегат прогресса
		Action<int> _progress;

		//токен для отмены
		CancellationToken _cancellationToken;

		//результат выполнения работы класса
		Result _cycleResult;

		//конструктор установит начальные значения полей класса
		public TransformCycle(IEnumerable<FileInfo> images, Action<int> progress,
			CancellationToken cancellationToken) {
			_images = images;
			_progress = progress;
			_cancellationToken = cancellationToken;
			_photos = new List<Photo>();
		}

		//асинхронный запуск работы класса
		public async Task ReadMetadataAsync() {

			//результат работы класса считается успешным, если пользователь не выбрал отмену
			try {
				await ConvertImagesToPhotosAsync();
				_cycleResult = Result.OK;
			} catch (OperationCanceledException) {
				_cycleResult = Result.Cancel;
			}
		}

		Task ConvertImagesToPhotosAsync() {
			return Task.Run(() => ConvertImagesToPhotos());
		}

		/*
		 * Основной метод класса
		 * 
		 * ...public для тестирования
		 */
		public void ConvertImagesToPhotos() {

			//прогресс всего импорта
			int index = 0;

			//для всех файлов изображений
			foreach (var image in _images) {

				//в случае отмены, отменять здесь
				_cancellationToken.ThrowIfCancellationRequested();

				//создать Photo
				var photo = new Photo(image);

				//добавить в результирующий список Photo
				_photos.Add(photo);

				//если дата снимка не распознана (некий неправильный jpeg)
				if (photo.DateString == Photo.DateNotRecognized) {

					//записать в лог, что есть файл, где не распознается дата
					log.Warn(string.Format(AppMessages.InconsistentImage, photo.Name));
				}

				//сообщить о прогрессе
				_progress(++index);
			}
		}

		public bool IsOK {
			get {
				return _cycleResult == Result.OK;
			}
		}

		public bool IsCancelled {
			get {
				return _cycleResult == Result.Cancel;
			}
		}

		public IEnumerable<Photo> Photos {
			get {
				return _photos;
			}
		}
	}
}
