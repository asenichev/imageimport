﻿using System;
using System.Diagnostics;
using System.IO;
using CSUtil;
using ImageImport.Models;

namespace ImageImport.Cycles {

	/*
	 * Проверяет доступность папки назначения и съемного диска и 
	 * формирует соответствующие объекты DirectoryInfo.
	 * 
	 * Ахтунг! Тесты этого класса используют диск "С:\" в качестве макета съемного диска,
	 * поэтому, если в класс будут вноситься изменения, которые приведут к какому-либо
	 * воздействию на диск (например записи пробных файлов или удаления чего либо),
	 * следует изменить тесты.
	 */
	public class PreparationCycle {

		//объект для хранения ошибок
		ErrorTrace _errorTrace;

		//данные папки источника
		DirectoryInfo _sourceFolder;

		//данные папки назначения
		DirectoryInfo _targetFolder;

		public PreparationCycle(string sourceFolder, string targetFolder) {

			_errorTrace = new ErrorTrace();

			//проверить съемный диск и записать поле _sourceFolder
			GetSourceFolder(sourceFolder);

			//проверить папку назначения и записать поле _targetFolder
			GetTargetFolder(targetFolder);
		}

		//съемный диск
		private void GetSourceFolder(string sourceFolder) {

			var sourceDisk = new SourceDisk(sourceFolder);

			//если диск распознан и карта вставлена
			if (sourceDisk.IsDiskIdentified && sourceDisk.IsCardInserted) {

				//зафиксировать данные папки
				_sourceFolder = sourceDisk.MainFolder;
			} else {

				//обработать ситуации отличные от приемлимой
				SetMessagesForDiskTroubles(sourceDisk);
			}
		}

		//если диск не готов
		private void SetMessagesForDiskTroubles(SourceDisk sourceDisk) {

			//если диск не распознан
			if (!sourceDisk.IsDiskIdentified) {

				//добавить сообщение в список ошибок
				_errorTrace.AddError(sourceDisk.ErrorMessage);

				//дальше смотреть нет смысла - диск не распознан
				return;
			}

			//если карта не вставлена
			if (!sourceDisk.IsCardInserted) {

				//добавить сообщение в список ошибок
				_errorTrace.AddError(string.Format(AppMessages.NoCardInserted, sourceDisk.DiskName));

			//невозможная ситуация
			} else {
				throw new ApplicationException(AppMessages.InvalidProgramState);
			}
		}

		//папка назначения
		private void GetTargetFolder(string targetFolderPath) {

			var targetFolder = new TargetFolder(targetFolderPath);

			//если папка не имеет волшебное название и доступна
			if (!targetFolder.IsNotSet && targetFolder.IsAccessible) {

				//зафиксировать данные папки
				_targetFolder = targetFolder.Directory;
			} else {

				//обработать все ситуации отличные от приемлимой
				SetMessagesForTargetFolderTroubles(targetFolder);
			}
		}

		void SetMessagesForTargetFolderTroubles(TargetFolder targetFolder) {

			//если папка не назначена
			if (targetFolder.IsNotSet) {

				//добавить соответствующее сообщение к списку ошибок
				_errorTrace.AddError(AppMessages.TargetFolderNotSet);

			//если папка не доступна (нет доступа к серверу и т.д.)
			} else if (!targetFolder.IsAccessible) {

				//добавить соответствующее сообщение к списку ошибок
				_errorTrace.AddError(targetFolder.ErrorMessage);

			//невозможная ситуация
			} else {
				throw new ApplicationException(AppMessages.InvalidProgramState);
			}
		}

		public bool IsReady {
			get {
				return _errorTrace.HasErrors == false;
			}
		}

		public string ErrorMessage {
			get {
				return _errorTrace.AllMessagesToString;
			}
		}

		public DirectoryInfo TargetFolder {
			get {
				return _targetFolder;
			}
		}

		public DirectoryInfo SourceFolder {
			get {
				return _sourceFolder;
			}
		}
	}
}
