﻿using System;
using System.Collections.Generic;
using ImageImport.Models;
using System.Linq;
using System.Security.Cryptography;

namespace ImageImport.Cycles {

	/*
	 * Переименовывает фотографии в пределах папки даты (папки содержащей снимки только одной даты)
	 * 
	 * Для каждой группы назначить префикс импорта - самое раннее время снимка,
	 * он обеспечивает соответствующую сортировку в папке.
	 * Примеры: 000512, 080001, 140206, 220405
	 * 
	 * Префикс и идентификатор должны быть разделены, иначе сортировка может нарушиться, если
	 * идентификатор начинается с цифры
	 * 
	 * Прибавить уникальный идентификатор импорта - он практически исключает совпадение
	 * различных импортов (в случае одинакового времени самого раннего снимка)
	 * Пример: 936DA01F
	 * 
	 * Рассортировать снимки по времени создания.
	 * 
	 * Добавить порядковый номер снимка в импорте в формате 0000
	 * 
	 * Сформировать строку с временем снимка в формате HHmmss - 
	 * Пример: 000116, 141500, 221647
	 * 
	 * Итоговое имя: 000512-936DA01F-0006-020316.jpg
	 * 
	 */

	public class Rename {

		static readonly string Delimeter = "-";
		static readonly string TimeTakenFormat = "HHmmss";
		static readonly string IndexNumberFormat = "{0:0000}";

		IList<Photo> _photos;

		public Rename(IEnumerable<Photo> photos, string importId) {

			//рассортировать по времени съемки
			_photos = photos.OrderBy(p => p.DateTimeTaken).ToList();

			//вычислить префикс импорта (наименьшее время из всех фотографий)
			string importPrefix = _photos.Min(p => p.DateTimeTaken).ToString(TimeTakenFormat);

			string formattedImportId = importId.ToUpper();

			for (int i = 0; i < _photos.Count; i++) {

				//префикс импорта, на выходе "000514-"
				_photos[i].Name = importPrefix + Delimeter;

				//идентификатор импорта, на выходе "000514936DA01F-"
				_photos[i].Name += formattedImportId + Delimeter;

				//порядковый номер, начиная с 1, на выходе "000514936DA01F-0002-"
				_photos[i].Name += string.Format(IndexNumberFormat, i + 1) + Delimeter;

				//время съемки, на выходе "000514936DA01F-0002-021403"
				_photos[i].Name += _photos[i].DateTimeTaken.ToString(TimeTakenFormat);

				//разрешение файла, на выходе 000514936DA01F-0002-021403.JPG"
				_photos[i].Name += AppConstants.JpgExtension;
			}
		}


		public IEnumerable<Photo> RenamedPhotos {
			get {
				return _photos;
			}
		}

	}
}
