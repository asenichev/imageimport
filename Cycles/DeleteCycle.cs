﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CSUtil;

namespace ImageImport.Cycles {

	/*
	 * Удаляет файлы из указанной директории
	 * 
	 * В зависимости от переданного флага, удаляет либо все файлы, либо
	 * только файлы, которые поддерживаются приложением (при этом все другие
	 * файлы и папки остаются)
	 * 
	 * Сначала класс создается, выполнение запускается отдельным асинхронным методом
	 */
	public class DeleteCycle {

		//создает класс для случая удаления файлов без ограничения
		public static DeleteCycle DeleteAllCycle(DirectoryInfo dir, 	Action<int> progress,
			CancellationToken cancellationToken){
			return new DeleteCycle(dir,false,progress,cancellationToken);
		}

		//создает класс для случая удаления только файлов изображений
		public static DeleteCycle DeleteImagesCycle(DirectoryInfo dir, Action<int> progress,
			CancellationToken cancellationToken) {
			return new DeleteCycle(dir, true, progress, cancellationToken);
		}

		//папка, из которой нужно удалить все подпапки и файлы
		DirectoryInfo _mainDirectory;

		//флаг, указывающий, нужно ли удалять все файлы или только поддерживаемые
		bool _deleteOnlyImageFiles;

		//счетчик прогресса
		int _progressCounter;

		//делегат прогресса
		Action<int> _progress;

		//токен для отмены
		CancellationToken _cancellationToken;

		//результат работы класса
		Result _deletionResult;

		//сообщение о возможной ошибке, которая могла возникнуть во время работы класса
		string _errorMessage;

		//конструктор инициирует поля
		DeleteCycle(DirectoryInfo dir, bool deleteOnlyImageFiles,
			Action<int> progress, CancellationToken cancellationToken) {
			_mainDirectory = dir;
			_deleteOnlyImageFiles = deleteOnlyImageFiles;
			_progress = progress;
			_cancellationToken = cancellationToken;
			_progressCounter = 0;
			_deletionResult = Result.Undefined;
		}

		//асинхронный запуск работы класса
		public Task DeleteFilesAsync() {
			return Task.Run(() => DeleteFiles());
		}

		//синхронный запуск работы класса
		private void DeleteFiles() {

			try {
				//по всем файлам и папкам в главной папке
				//запустить рекурсивный метод удаления
				foreach (var fsi in _mainDirectory.GetFileSystemInfos())
					DeleteFileSystemInfo(fsi);

				//если не было исключений, результат работы класса считается ОК
				_deletionResult = Result.OK;

				//если операция была отменена пользователем
			} catch (OperationCanceledException) {

				//результат работы класса - отмена
				_deletionResult = Result.Cancel;

				//возможное исключение (сделано для случая, если съемный диск защищен от записи)
			} catch (IOException e) {

				//резултат работы класса - ошибка
				_deletionResult = Result.Error;

				//зафиксировать сообщение об ошибке
				_errorMessage = e.Message;
			}
		}

		/*
		 * Рекурсивное удаление файлов и папок
		 * 
		 * ...based on http://stackoverflow.com/questions/611921
		 */
		private void DeleteFileSystemInfo(FileSystemInfo fileSystemInfo) {

			//в случае отмены останавливаемся здесь
			_cancellationToken.ThrowIfCancellationRequested();

			//смотрим, не папка ли
			var directoryInfo = fileSystemInfo as DirectoryInfo;

			//если папка
			if (directoryInfo != null) {

				//для всех FileInfo в папке
				foreach (var childInfo in directoryInfo.GetFileSystemInfos()) {

					//удаляем содержимое
					DeleteFileSystemInfo(childInfo);
				}
			}

			//если нужно удалять только поддерживаемые файлы
			if (_deleteOnlyImageFiles) {

				//проверяем расширение объекта FileSystemInfo
				if (AppConstants.SupportedExtension.IsMatch(fileSystemInfo.Name)) {

					//удаляем FileSystemInfo, если расширение совпадает
					fileSystemInfo.Delete();

					//фиксируем удаление
					SetObjectDeleted();
				}

			//если нет ограничений на удаление
			} else {

				//для случая скрытых файлов, устанавливаем атрибут "нормальный"
				//чтобы можно было удалить
				fileSystemInfo.Attributes = FileAttributes.Normal;

				//удаляем FileSystemInfo
				fileSystemInfo.Delete();

				//фиксируем удаление
				SetObjectDeleted();
			}
		}

		//фиксирует удаление файла (папки)
		private void SetObjectDeleted() {

			//прибавляем к счетчику единицу
			_progressCounter++;

			//сообщаем о прогрессе
			_progress(_progressCounter);
		}

		public bool IsOK {
			get {
				return _deletionResult == Result.OK;
			}
		}

		public bool IsCancelled {
			get {
				return _deletionResult == Result.Cancel;
			}
		}

		public bool IsError {
			get {
				return _deletionResult == Result.Error;
			}
		}

		public string ErrorMessage {
			get {
				return _errorMessage;
			}
		}

		public int Count {
			get {
				return _progressCounter;
			}
		}

	}
}
