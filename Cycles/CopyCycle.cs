﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CSUtil;
using ImageImport.Models;

namespace ImageImport.Cycles {

	/*
	 * Копирует файлы изображений в назначенную папку
	 * 
	 * Переименование файлов происходит здесь, поскольку здесь оно разделено на части и не очень
	 * заметно замедляет индикатор прогресса.
	 * Если делать переименование где-то еще, индикатор будет возможно останавливаться на заметное
	 * время, а расчитать заранее объем переименований - слишком усложнит программу и замедлит
	 * быстродействие на старте
	 * 
	 * Во время копирования проводится проверка размера файлов - для того, чтобы выдавать
	 * предупреждение о слишком больших файлах только тогда, когда они фактически скопированы
	 * 
	 * Класс сначала создается, запускается отдельно асинхронным методом.
	 */
	public class CopyCycle {

		//Список объектов метаданных изображений
		IEnumerable<Photo> _photos;

		//названия папок, в которых присутствуют большие изображения
		ISet<string> _oversized;

		//назначенная папка
		DirectoryInfo _targetDirectory;

		//счетчик сообщений о прогрессе
		int _progressCounter;

		//для сообщений о прогрессе
		Action<int> _progress;

		//для возможной отмены
		CancellationToken _cancellationToken;

		//результат выполнения всего копирования
		Result _copyResult;

		//идентификатор импорта
		string _importId;

		//подробные результаты копирования одной строкой
		string _report;


		public CopyCycle(IEnumerable<Photo> photos, DirectoryInfo targetDir, string importId,
			Action<int> progress, CancellationToken cancellationToken) {

			_photos = photos;
			_oversized = new HashSet<string>();
			_targetDirectory = targetDir;
			_progress = progress;
			_cancellationToken = cancellationToken;
			_copyResult = Result.Undefined;
			_report = string.Empty;
			_importId = importId;
		}

		//основной асинхронный метод класса
		public Task CopyImagesAsync() {
			return Task.Run(() => Copy());
		}

		/*
		 * Результат выполнения положителен, если копирование прошло
		 * без отмены пользователем
		 */
		void Copy() {
			try {
				CopyToFolders();
				_copyResult = Result.OK;
			} catch (OperationCanceledException) {
				_copyResult = Result.Cancel;
			}
		}

		/*
		 * Фотографии группируются по дате изображения,
		 * затем, для каждой группы находится (или создается если не было)
		 * папка в директории назначения.
		 * Затем фотографии переименовываются и копируются в эту папку.
		 */
		void CopyToFolders() {

			//группировка по дате
			var groupedByDay = from photo in _photos
							   group photo by photo.DateString;

			//для каждой группы (т.е. даты)
			foreach (var group in groupedByDay.OrderBy(g => g.Key)) {

				//создается или находится папка даты
				var dayFolder = GetOrCreateTargetFolder(group.Key);

				//переименовываются фотографии
				var consistentNames = RenamePhotos(group, dayFolder);

				//копируются фотографии в папку даты
				CopyToDateFolder(dayFolder, consistentNames);
			}
		}

		//создается или находится папка даты
		private DirectoryInfo GetOrCreateTargetFolder(string folderName) {

			string result = Path.Combine(_targetDirectory.FullName, folderName);

			if (!Directory.Exists(result))
				Directory.CreateDirectory(result);

			return new DirectoryInfo(result);
		}

		//назначает фотографиям имена, которые обеспечат удобную сортировку в папке
		private IEnumerable<Photo> RenamePhotos(IEnumerable<Photo> photos, DirectoryInfo dayFolder) {
			
			//если имя папки даты снимка равно волшебной константе
			if (dayFolder.Name == Photo.DateNotRecognized) {

				//запускается специальное (грубое) переименование
				return new RenameOdd(photos).RenamedPhotos;

			//если имя папки даты снимка не равно волшебной константе
			} else {

				//запускается обычное переименование
				return new Rename(photos, _importId).RenamedPhotos;
			}
		}

		//копирует фотографии в папку даты и фиксирует результаты
		private void CopyToDateFolder(DirectoryInfo dayFolder, IEnumerable<Photo> consistentNames) {

			//количество скопированных фотографий для данной папки
			int exported = 0;

			try {

				//копировать в папку даты
				ExportToFolder(consistentNames, dayFolder, out exported);

				//если пользователь выбрал отмену
			} catch (OperationCanceledException) {

				//перебрасываем исключение, но сперва записываем результаты в отчет класса 
				throw;

			} finally {

				//строка отчета 
				string reportLine = string.Format(AppMessages.ImportedTo, dayFolder.Name, exported,
					AppMessages.ImagesRu(exported));

				//добавляем к общему отчету класса строку
				if (_report.Length != 0)
					_report += AppMessages.NewLine;
				_report += reportLine;
			}
		}

		//копирует файлы в указанную папку, фиксирует слишком большие файлы
		private void ExportToFolder(IEnumerable<Photo> photos, DirectoryInfo dayFolder, out int localCount) {

			//количество скопированных фотографий для данной папки
			localCount = 0;

			foreach (var photo in photos) {

				//отмена возможна на каждом шаге
				_cancellationToken.ThrowIfCancellationRequested();

				//создать путь файла на основе имени папки дня и имени файла
				var targetPath = Path.Combine(dayFolder.FullName, photo.Name);

				//копировать файл
				File.Copy(photo.FullName, targetPath);

				//если размер файла превышает допустимый
				if (photo.FileSize > AppConstants.MaxSize) {

					//занести файл в список слишком больших файлов
					_oversized.Add(photo.DateString);
				}

				//увеличить счетчик общего прогресса класса
				_progressCounter++;

				//сообщить о прогрессе
				_progress(_progressCounter);

				//увеличить счетчик для данной папки
				localCount++;
			}
		}

		public bool IsOK {
			get {
				return _copyResult == Result.OK;
			}
		}

		public bool IsCancelled {
			get {
				return _copyResult == Result.Cancel;
			}
		}

		public int Count {
			get {
				return _progressCounter;
			}
		}

		public bool HasOversized {
			get {
				return _oversized.Count > 0;
			}
		}

		//список папок, в которых есть слишком большие файлы
		//каждое имя папки пишется отдельной строкой,
		//в конце лишнего переноса строки нет
		public string OversizedFolders {

			get {

				//для возможность доступа по индексу преобразуем в список
				var asList = _oversized.ToList();

				var b = new StringBuilder();

				//от первого значения для предпоследнего
				for (int i = 0; i < asList.Count - 1; i++)

					//добавляем с переносом строки
					b.AppendLine(asList[i]);

				//проверяем, что вообще есть, что добавлять
				if (asList.Count > 0)

					//последнее значение добавляем без переноса строки
					b.Append(asList[asList.Count - 1]);

				return b.ToString();
			}
		}

		public string ImportId {
			get {
				return _importId;
			}
		}

		public string Report {
			get {
				return _report;
			}
		}

	}
}
