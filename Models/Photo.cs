﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ImageImport.Models {

	/*
	 * Содержит набор мета-данных изображения нужный для дальнейшего импорта.
	 */

	public class Photo {

		static readonly Regex DateConversionRegex = new Regex(":");
		static readonly Regex DateRegex = new Regex(@"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d");
		static readonly string DateDelimeter = "-";
		static readonly char CharSpace = ' ';
		static readonly char CharT = 'T';
		static readonly int DateDelimetersCount = 2;
		static readonly int CreationTimePropertyId = 36867;
		public static readonly string DateNotRecognized = "Дата неопознана";

		string _name;
		string _fullName;
		DateTime _dateTime;
		string _dateString;
		long _fileSize;

		public Photo(FileInfo fileInfo) {
			_name = fileInfo.Name;
			_fullName = fileInfo.FullName;
			_fileSize = fileInfo.Length;
			ReadDate();
		}

		void ReadDate() {
			using (FileStream fs = new FileStream(_fullName, FileMode.Open, FileAccess.Read)) {
				try {
					ReadFromImage(fs);
				} catch (ArgumentException) {
					//AgrumentException выбрасывается если файл на самом деле не изображение
					//а просто имеет расширение .jpg
					_dateString = DateNotRecognized;
					_dateTime = DateTime.MinValue;
				}
			}
		}

		void ReadFromImage(FileStream fs) {
			using (Image myImage = Image.FromStream(fs, false, false)) {
				if (myImage.PropertyIdList.Any(p => p == CreationTimePropertyId)) {
					PropertyItem propItem = myImage.GetPropertyItem(CreationTimePropertyId);
					var dateAsString = ToSortableDateTimePattern(propItem);
					//"2013-01-01T13:14:15"
					_dateString = dateAsString.Split(CharT)[0];
					_dateTime = DateTime.Parse(dateAsString);
				} else {
					_dateString = DateNotRecognized;
				}
			}
		}

		/*
		 * Преобразовывает свойство вида "2013:01:01 13:14:15/0"
		 * в формат "2013-01-01T13:14:15",
		 * который можно затем преобразовать в DateTime.
		 */
		string ToSortableDateTimePattern(PropertyItem propertyItem) {
			string stringValue = Encoding.UTF8.GetString(propertyItem.Value);
			//"2013:01:01 13:14:15/0"
			string correctedDate = DateConversionRegex.Replace(stringValue,
				DateDelimeter, DateDelimetersCount);
			//"2013-01-01 13:14:15/0"
			string correctedTime = DateRegex.Match(correctedDate).Value;
			//"2013-01-01 13:14:15/0";
			return correctedTime.Replace(CharSpace, CharT);
		}

		public string Name { get { return _name; } set { _name = value; } }
		public string FullName { get { return _fullName; } }
		public string DateString { get { return _dateString; } }
		public DateTime DateTimeTaken { get { return _dateTime; } }
		public long FileSize { get { return _fileSize; } }
	}
}
