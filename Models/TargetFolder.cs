﻿using System;
using System.IO;

namespace ImageImport.Models {

	/*
	 * Инкапсулирует данные папки приема изображений
	 * 
	 * Ахтунг! Тесты этого класса используют диск "С:\" в качестве макета съемного диска,
	 * поэтому, если в класс будут вноситься изменения, которые приведут к какому-либо
	 * воздействию на диск (например записи пробных файлов или удаления чего либо),
	 * следует изменить тесты.
	 * 
	 */
	public class TargetFolder {

		//данные папки
		DirectoryInfo _directoryInfo;

		//флаг, подтверждающий, что папка доступна
		bool _isAccessible;

		//сообщение об ошибке для случая недоступной папки
		string _errorMessage;

		public TargetFolder(string path) {

			//объект DirectoryInfo создается с проверкой на неправильные значения пути
			try {
				//создать объект
				_directoryInfo = new DirectoryInfo(path);
				
				//проверить доступ к папке
				CheckAccessibility();

			//для случая неправильных входных параметров конструктора
			} catch (Exception e) {
				_isAccessible = false;
				_errorMessage = e.Message;
			}
		}

		//Проверяет доступ к папке
		private void CheckAccessibility() {
			if (_directoryInfo.Exists) {
				_isAccessible = true;
				_errorMessage = string.Empty;
			} else {
				_isAccessible = false;
				_errorMessage = AppMessages.FolderNotAccessible + _directoryInfo.FullName;
			}
		}

		public DirectoryInfo Directory {
			get {
				return _directoryInfo;
			}
		}

		public bool IsAccessible {
			get {
				return _isAccessible;
			}
		}

		public bool IsNotSet {
			get {
				if (_directoryInfo == null)
					return false;
				return _directoryInfo.Name == AppConstants.Unknown;
			}
		}

		public string ErrorMessage {
			get {
				return _errorMessage;
			}
		}
	}
}
