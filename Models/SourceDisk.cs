﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageImport.Models {

	/*
	 * Инкапсулирует данные диска - источника фотографий
	 * 
	 * Ахтунг! Тесты этого класса используют диск "С:\" в качестве макета съемного диска,
	 * поэтому, если в класс будут вноситься изменения, которые приведут к какому-либо
	 * воздействию на диск (например записи пробных файлов или удаления чего либо),
	 * следует изменить тесты.
	 */
	public class SourceDisk {

		//данные диска
		DriveInfo _disk;

		//флаг, подтверждающий, что диск идентифицирован программой
		bool _isIdentified;

		//сообщение об ошибке для случая если диск не распознан
		string _errorMessage;

		public SourceDisk(string diskName) {
			_errorMessage = string.Empty;
			try {
				_disk = new DriveInfo(diskName);
				_isIdentified = true;
			} catch {
				_errorMessage = string.Format(AppMessages.CardNotIdentified, diskName);
				_isIdentified = false;
			}
		}

		public bool IsDiskIdentified {
			get {
				return _isIdentified;
			}
		}

		public bool IsCardInserted {
			get {
				if (_disk == null)
					return false;
				return _disk.IsReady;
			}
		}

		public string DiskName {
			get {
				if (_disk == null)
					return string.Empty;
				return _disk.Name;
			}
		}

		public DirectoryInfo MainFolder {
			get {
				if (_disk == null)
					return null;
				return _disk.RootDirectory;
			}
		}

		public string ErrorMessage {
			get {
				return _errorMessage;
			}
		}
	}
}
