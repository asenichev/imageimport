﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ImageImport.Cycles;
using ImageImport.MainWindows;
using log4net;

namespace ImageImport.States {

	class Transform : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;
		CancellationTokenSource _cancelTokenSource;

		public Transform(MainWindowVM program) {
			_program = program;
			_cancelTokenSource = new CancellationTokenSource();
		}

		public async void Invoke() {
			var transformCycle = new TransformCycle(_program.ImageFiles, ReportProgress,
				_cancelTokenSource.Token);
			await transformCycle.ReadMetadataAsync();
			if (transformCycle.IsOK) {
				_program.Photos = transformCycle.Photos;
				_program.SetState(new Copy(_program));
			} else if (transformCycle.IsCancelled) {
				log.Info(AppMessages.CancelledAtTransform);
				_program.SetState(new FinishedTransformCancelled());
			} else {
				throw new ApplicationException(AppMessages.InvalidState);
			}
		}

		void ReportProgress(int progress) {
			_program.ReportProgress(progress);
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return false; }
		}

		public bool CanCancel {
			get { return true; }
		}

		public void Cancel() {
			_cancelTokenSource.Cancel();
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Progress; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Undefined; }
		}

		public string MainInstruction {
			get {
				return string.Format(AppMessages.ImportStarted, _program.ImageFilesCount,
					AppMessages.ImagesRuNone(_program.ImageFilesCount));
			}
		}

		public string SupInstruction {
			get { return AppMessages.TransformProgress; }
		}

		public bool CanChangeFolders {
			get { return false; }
		}
	}
}
