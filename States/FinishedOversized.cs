﻿using ImageImport.MainWindows;
using log4net;
using m = ImageImport.AppMessages;

namespace ImageImport.States {

	class FinishedOversized : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;

		public FinishedOversized(MainWindowVM program) {
			_program = program;
		}

		public void Invoke() {
			log.Info(SupInstruction);
		}

		public string MainInstruction {
			get { return AppMessages.ImportSucceed; }
		}

		public string SupInstruction {
			get {
				var result = _program.CopyReport;
				result += m.NewLine + m.NewLine;
				result += OversizedInstruction(_program.OversizedFolders);
				if (_program.HasUnsupportedMedia) {
					result += m.NewLine + m.NewLine;
					result += FinishedUnsupportedMedia.UnsupportedMediaInstruction(_program.UnsupportedMedia);
				}
				return result;
			}
		}

		public static string OversizedInstruction(string details) {
			return string.Format(m.OversizedImages, m.MaxSizeToString(), details);
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return true; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Message; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Warning; }
		}

		public bool CanChangeFolders {
			get { return true; }
		}
	}
}
