﻿using ImageImport.MainWindows;
using log4net;
using m = ImageImport.AppMessages;

namespace ImageImport.States {

	class FinishedCopyCancelled : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;

		public FinishedCopyCancelled(MainWindowVM program) {
			_program = program;
		}

		public void Invoke() {
			log.Info(SupInstruction);
		}

		public string SupInstruction {
			get {
				var result = _program.CopyReport;
				result += m.NewLine + m.NewLine;
				result += m.NoImagesDeleted;
				if (_program.HasOversizedImages) {
					result += m.NewLine + m.NewLine;
					result += FinishedOversized.OversizedInstruction(_program.OversizedFolders);
				}
				return result;
			}
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return true; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Message; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Warning; }
		}

		public string MainInstruction {
			get { return AppMessages.ExecutionStopped; }
		}

		public bool CanChangeFolders {
			get { return true; }
		}
	}
}
