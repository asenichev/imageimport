﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CSUtil;
using ImageImport.MainWindows;

namespace ImageImport.States {

	public interface IState {
		void Invoke();
		void OnClosing(CancelEventArgs e);
		bool CanRestart { get; }
		bool CanCancel { get; }
		bool CanChangeFolders { get; }
		void Cancel();
		Page ActivePage { get; }
		Result Result { get; }
		string MainInstruction { get; }
		string SupInstruction { get; }
	}
}
