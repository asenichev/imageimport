﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageImport.Cycles;
using ImageImport.MainWindows;
using log4net;

namespace ImageImport.States {

	class Prepare : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private MainWindowVM _program;

		public Prepare(MainWindowVM program) {
			_program = program;
		}

		public void Invoke() {
			var source = Properties.Settings.Default.DiskName;
			var target = Properties.Settings.Default.TargetFolder;
			var preparationCycle = new PreparationCycle(source, target);
			if (preparationCycle.IsReady) {
				_program.SourceFolder = preparationCycle.SourceFolder;
				_program.TargetFolder = preparationCycle.TargetFolder;
				_program.SetState(new Search(_program));
			} else {
				_program.SetState(new FinishedNotReady(preparationCycle.ErrorMessage));
			}
		}

		private void LogSourceAndTarget(DirectoryInfo source, DirectoryInfo target) {
			log.Info(string.Format(AppMessages.ImageSource, source.FullName));
			log.Info(string.Format(AppMessages.ImageTarget, target.FullName));
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//let close
		}

		public bool CanRestart {
			get { return false; }
		}

		public bool CanCancel {
			get { return true; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Undefined; }
		}

		public string MainInstruction {
			get { return string.Empty; }
		}

		public string SupInstruction {
			get { return string.Empty; }
		}

		public void Cancel() {
			throw new NotImplementedException();
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Progress; }
		}

		public bool CanChangeFolders {
			get { return false; }
		}
	}
}
