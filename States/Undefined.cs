﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageImport.States {

	class Undefined : IState {

		public int Copied {
			get { return 0; }
		}

		public int Deleted {
			get { return 0; }
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//позволить закрыть окно
		}

		public bool CanRestart {
			get { return true; }
		}

		public void Cancel() {
			throw new NotImplementedException();
		}
		public bool CanCancel {
			get { return false; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Undefined; }
		}

		public string MainInstruction {
			get { return string.Empty; }
		}

		public string SupInstruction {
			get { return string.Empty; }
		}

		public void Invoke() {
			//не должно использоваться в этом состоянии
			throw new NotImplementedException();
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Progress; }
		}

		public bool CanChangeFolders {
			get { return false; }
		}
	}
}
