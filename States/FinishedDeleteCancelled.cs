﻿using CSUtil;
using ImageImport.MainWindows;
using log4net;
using m = ImageImport.AppMessages;

namespace ImageImport.States {

	class FinishedDeleteCancelled : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;

		public FinishedDeleteCancelled(MainWindowVM program) {
			_program = program;
		}

		public void Invoke() {
			log.Info(SupInstruction);
		}

		public string SupInstruction {
			get {
				var result = m.NoRestartWarning;
				result += m.NewLine + m.NewLine;
				result += FilesDeleted(_program.DeletedCount);
				result += m.NewLine + m.NewLine;
				result += _program.CopyReport;
				if (_program.HasOversizedImages) {
					result += m.NewLine + m.NewLine;
					result += FinishedOversized.OversizedInstruction(_program.OversizedFolders);
				}
				if (_program.HasUnsupportedMedia) {
					result += m.NewLine + m.NewLine;
					result += FinishedUnsupportedMedia.UnsupportedMediaInstruction(_program.UnsupportedMedia);
				}
				return result;
			}
		}

		public static string FilesDeleted(int count) {
			return string.Format(m.FilesDeleted, CorrectWord(count), count, CorrectQuantifier(count));
		}

		private static string CorrectWord(int count) {
			return StringUtil.UppercaseFirst(m.DeletedMaleRu(count));
		}

		private static string CorrectQuantifier(int count) {
			return m.Files(count);
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return false; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Message; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Warning; }
		}

		public string MainInstruction {
			get { return AppMessages.ExecutionStopped; }
		}

		public bool CanChangeFolders {
			get { return true; }
		}
		
	}
}
