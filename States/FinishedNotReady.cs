﻿using log4net;

namespace ImageImport.States {

	class FinishedNotReady : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		string _reasonMessage;

		public FinishedNotReady(string reasonMessage) {
			_reasonMessage = reasonMessage;
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return true; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Message; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Error; }
		}

		public string MainInstruction {
			get { return AppMessages.ExecutionNotPossible; }
		}

		public string SupInstruction {
			get { return _reasonMessage; }
		}

		public void Invoke() {
			log.Info(string.Format(AppMessages.ExecutionNotPossibleLogInfo, _reasonMessage));
		}

		public bool CanChangeFolders {
			get { return true; }
		}
	}
}
