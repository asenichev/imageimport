﻿using ImageImport.Cycles;
using ImageImport.MainWindows;
using log4net;

namespace ImageImport.States {

	class Search : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;

		public Search(MainWindowVM program) {
			_program = program;
		}

		public void Invoke() {
			var searchCycle = new SearchCycle(_program.SourceFolder);
			if (searchCycle.ImageFilesCount > 0) {
				LogImagesCount(searchCycle.ImageFilesCount);
				_program.ImageFiles = searchCycle.ImageFiles;
				_program.ImageFilesCount = searchCycle.ImageFilesCount;
				_program.HasUnsupportedMedia = searchCycle.HasUnsupportedMedia;
				_program.UnsupportedMedia = searchCycle.UnsupportedMediaList;
				_program.SetProgressMax(searchCycle.ImageFilesCount, searchCycle.AllFilesCount, searchCycle.MediaFilesCount);
				_program.SetState(new Transform(_program));
			} else {
				_program.SetState(new FinishedNoImages());
			}
		}

		private void LogImagesCount(int imageCount) {
			log.Info(string.Format(AppMessages.ImagesToImport, imageCount, AppMessages.ImagesRu(imageCount)));
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return false; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Progress; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Undefined; }
		}

		public string MainInstruction {
			get { return string.Empty; }
		}

		public string SupInstruction {
			get { return string.Empty; }
		}

		public bool CanChangeFolders {
			get { return false; }
		}
	}
}
