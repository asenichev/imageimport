﻿using ImageImport.MainWindows;
using log4net;
using m = ImageImport.AppMessages;

namespace ImageImport.States {

	class FinishedDeleteError : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;

		public FinishedDeleteError(MainWindowVM program) {
			_program = program;
		}

		public void Invoke() {
			log.Info(SupInstruction);
		}

		public string SupInstruction {
			get {
				var result = m.NoRestartWarning;
				result += m.NewLine + m.NewLine;
				result += FinishedDeleteCancelled.FilesDeleted(_program.DeletedCount);
				result += m.NewLine + m.NewLine;
				result += _program.CopyReport;
				if (_program.HasOversizedImages) {
					result += m.NewLine + m.NewLine;
					result += FinishedOversized.OversizedInstruction(_program.OversizedFolders);
				}
				return result;
			}
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return false; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Message; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Warning; }
		}

		public string MainInstruction {
			get { return AppMessages.ErrorAtDeleting; }
		}

		public bool CanChangeFolders {
			get { return true; }
		}
		
	}
}
