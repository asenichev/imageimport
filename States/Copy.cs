﻿using System;
using System.Threading;
using ImageImport.Cycles;
using ImageImport.MainWindows;
using log4net;

namespace ImageImport.States {

	class Copy : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;
		CancellationTokenSource _cancelTokenSource;

		public Copy(MainWindowVM program) {
			_program = program;
			_cancelTokenSource = new CancellationTokenSource();
		}

		public async void Invoke() {
			var copyCycle = new CopyCycle(_program.Photos, _program.TargetFolder, _program.ImportId, ReportProgress, _cancelTokenSource.Token);
			LogImportId(copyCycle.ImportId);
			await copyCycle.CopyImagesAsync();
			_program.CopyReport = copyCycle.Report;
			_program.HasOversizedImages = copyCycle.HasOversized;
			_program.OversizedFolders = copyCycle.OversizedFolders;
			LogCopyResults(copyCycle.Count);
			if (copyCycle.IsOK) {
				_program.SetState(new Delete(_program));
			} else if (copyCycle.IsCancelled) {
				log.Info(AppMessages.CancelledAtCopy);
				_program.SetState(new FinishedCopyCancelled(_program));
			} else {
				throw new ApplicationException(AppMessages.InvalidState);
			}
		}

		void ReportProgress(int progress) {
			_program.CopiedCount = progress;
			_program.ReportProgress(_program.ImageFilesCount + progress);
		}

		private void LogImportId(string importId) {
			log.Info(string.Format(AppMessages.ImportId, importId));
		}

		private void LogCopyResults(int count) {
			log.Info(string.Format(AppMessages.TotalCopied, count, AppMessages.ImagesRu(count)));
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			e.Cancel = true;
		}

		public bool CanRestart {
			get { return false; }
		}

		public bool CanCancel {
			get { return true; }
		}

		public void Cancel() {
			_cancelTokenSource.Cancel();
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Progress; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Undefined; }
		}

		public string MainInstruction {
			get {
				return string.Format(AppMessages.ImportStarted, _program.ImageFilesCount,
					AppMessages.ImagesRuNone(_program.ImageFilesCount));
			}
		}

		public string SupInstruction {
			get { return AppMessages.CopyProgress; }
		}


		public bool CanChangeFolders {
			get { return false; }
		}
	}
}
