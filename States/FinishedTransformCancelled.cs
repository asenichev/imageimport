﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace ImageImport.States {

	class FinishedTransformCancelled : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		
		public void Invoke() {
			log.Info(AppMessages.CancelledAtTransform);
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return true; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Message; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Warning; }
		}

		public string MainInstruction {
			get { return AppMessages.ExecutionStopped; }
		}

		public string SupInstruction {
			get { return AppMessages.ImagesAfterTransformCancelled; }
		}

		public bool CanChangeFolders {
			get { return true; }
		}
	}
}
