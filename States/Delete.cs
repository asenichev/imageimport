﻿using System;
using System.Threading;
using CSUtil;
using ImageImport.Cycles;
using ImageImport.MainWindows;
using log4net;

namespace ImageImport.States {

	class Delete : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;
		CancellationTokenSource _cancelTokenSource;

		public Delete(MainWindowVM program) {
			_program = program;
			_cancelTokenSource = new CancellationTokenSource();
		}

		public async void Invoke() {
			DeleteCycle deleteCycle;
			if (_program.HasUnsupportedMedia) {
				deleteCycle = DeleteCycle.DeleteImagesCycle(_program.SourceFolder, ReportProgress,
					_cancelTokenSource.Token);
			} else {
				deleteCycle = DeleteCycle.DeleteAllCycle(_program.SourceFolder, ReportProgress,
					_cancelTokenSource.Token);
			}
			await deleteCycle.DeleteFilesAsync();
			_program.ErrorAtDelete = deleteCycle.ErrorMessage;
			LogDeleteResults(deleteCycle.Count);
			if (deleteCycle.IsOK && !_program.HasOversizedImages && !_program.HasUnsupportedMedia) {
				_program.SetState(new FinishedOK(_program));
			} else if (deleteCycle.IsError) {
				LogError(deleteCycle.ErrorMessage);
				_program.SetState(new FinishedDeleteError(_program));
			} else if (deleteCycle.IsCancelled) {
				log.Info(AppMessages.CancelledAtDelete);
				_program.SetState(new FinishedDeleteCancelled(_program));
			} else if (_program.HasOversizedImages) {
				_program.SetState(new FinishedOversized(_program));
			} else if (_program.HasUnsupportedMedia) {
				_program.SetState(new FinishedUnsupportedMedia(_program));
			} else {
				throw new ApplicationException(AppMessages.InvalidState);
			}
		}

		void ReportProgress(int progress) {
			_program.DeletedCount = progress;
			_program.ReportProgress(_program.ImageFilesCount * 2 + progress);
		}

		private void LogDeleteResults(int deletedCount) {
			if (_program.HasUnsupportedMedia) {
				log.Info(string.Format(AppMessages.FilesDeleted,
					StringUtil.UppercaseFirst(AppMessages.DeletedMaleRu(deletedCount)), deletedCount,
					AppMessages.Files(deletedCount)));
			} else {
				log.Info(string.Format(AppMessages.FilesDeleted,
					StringUtil.UppercaseFirst(AppMessages.DeletedMaleRu(deletedCount)), deletedCount,
					AppMessages.Files(deletedCount)));
			}
		}

		private void LogError(string message) {
			log.Warn(message);
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			e.Cancel = true;
		}

		public bool CanRestart {
			get { return false; }
		}

		public bool CanCancel {
			get { return true; }
		}

		public void Cancel() {
			_cancelTokenSource.Cancel();
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Progress; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Undefined; }
		}

		public string MainInstruction {
			get {
				return string.Format(AppMessages.ImportStarted, _program.ImageFilesCount,
					AppMessages.ImagesRuNone(_program.ImageFilesCount));
			}
		}

		public string SupInstruction {
			get { return AppMessages.DeleteProgress; }
		}


		public bool CanChangeFolders {
			get { return false; }
		}
	}
}
