﻿using ImageImport.MainWindows;
using log4net;
using m = ImageImport.AppMessages;

namespace ImageImport.States {

	class FinishedUnsupportedMedia : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;

		public FinishedUnsupportedMedia(MainWindowVM program) {
			_program = program;
		}

		public void Invoke() {
			log.Info(SupInstruction);
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return true; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Message; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.Warning; }
		}

		public string MainInstruction {
			get { return AppMessages.UnsupportedFiles; }
		}

		public string SupInstruction {
			get {
				var result = _program.CopyReport;
				result += m.NewLine + m.NewLine;
				result += UnsupportedMediaInstruction(_program.UnsupportedMedia);
				return result;
			}
		}

		public static string UnsupportedMediaInstruction(string details) {
			return string.Format(m.UnsupportedFilesDetailed, AppConstants.SupportedExtensionName, details);
		}

		public bool CanChangeFolders {
			get { return true; }
		}
	}
}
