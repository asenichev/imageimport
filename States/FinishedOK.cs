﻿using ImageImport.MainWindows;
using log4net;

namespace ImageImport.States {

	class FinishedOK : IState {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindowVM _program;
		public FinishedOK(MainWindowVM program) {
			_program = program;
		}

		public void Invoke() {
			LogSuccessfulImport();
		}

		private void LogSuccessfulImport() {
			log.Info(SupInstruction);
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			//ok
		}

		public bool CanRestart {
			get { return true; }
		}

		public bool CanCancel {
			get { return false; }
		}

		public void Cancel() {
			//nil
		}

		public MainWindows.Page ActivePage {
			get { return MainWindows.Page.Message; }
		}

		public CSUtil.Result Result {
			get { return CSUtil.Result.OK; }
		}

		public string MainInstruction {
			get { return AppMessages.ImportSucceed; }
		}

		public string SupInstruction {
			get { return _program.CopyReport; }
		}

		public bool CanChangeFolders {
			get { return true; }
		}
	}
}
