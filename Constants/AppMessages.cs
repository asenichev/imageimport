﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ImageImport {

	public class AppMessages {
		public static readonly string NewLine = "\n";
		public static readonly string UnexpectedError = "Непредвиденная ошибка";
		public static readonly string CopiedDeleted = "\nСкопировано изображений: {0}\nУдалено файлов: {1}";
		public static readonly string GeneralErrorSupInstruction = "Содержание ошибки: {0}";
		public static readonly string TargetFolderNotSet = "Не выбрана папка, в которую должны экспортироваться изображения.";
		public static readonly string FolderNotAccessible = "Нет доступа к ";
		public static readonly string CardNotIdentified = "Не распознан съемный диск: {0}";
		public static readonly string NoCardInserted = "Съемный диск ({0}) - карта не обнаружена";
		public static readonly string NoCardSelected = "Не выбран съемный диск.";
		public static readonly string InvalidProgramState = "Невозможное состояние программы";
		public static readonly string ExecutionNotPossible = "Выполнение невозможно";
		public static readonly string ExecutionNotPossibleLogInfo = "Импорт невозможен: {0}";
		public static readonly string ImageSource = "Источник изображений: {0}";
		public static readonly string NoImagesFound = "Изображений не обнаружено";
		public static readonly string ImageTarget = "Папка назначения: {0}";
		public static readonly string InconsistentImage = "Не удалось распознать дату съемки: {0}";
		public static readonly string ImportId = "Идентификатор импорта: {0}";
		public static readonly string ExecutionCancelled = "Выполнение отменено";
		public static readonly string ImportStarted = "Выполняется импорт {0} {1}";
		public static readonly string ImagesToImport = "Всего найдено {0} {1}.";
		public static readonly string TotalCopied = "Всего скопировано {0} {1}.";
		public static readonly string TransformProgress = "Подготовка...";
		public static readonly string CopyProgress = "Копирование...";
		public static readonly string DeleteProgress = "Очистка карты...";
		public static readonly string ExecutionStopped = "Выполнение остановлено";
		public static readonly string UnsupportedFiles = "Фотографии успешно импортированы, и удалены с карты";
		public static readonly string UnsupportedFilesDetailed = "Программа работает только с файлами {0}.\nСписок медиафайлов, которые не импортировались и не были удалены:\n{1}";
		public static readonly string CancelledAtTransform = "Выполнение отменено пользователем на стадии считывания данных.";
		public static readonly string CancelledAtCopy = "Выполнение отменено пользователем на стадии копирования изображений.";
		public static readonly string CancelledAtDelete = "Выполнение отменено пользователем на стадии удаления файлов.";
		public static readonly string ErrorAtDeleteLog = "Ошибка при удалении файлов: {0}";
		public static readonly string UnsupportedFilesLogInfo = "Удаление не осуществлено, поскольку обнаружены неподдерживамые медиа файлы: {0}";
		public static readonly string ImportedTo = "В папку \"{0}\" импортировано {1} {2}";
		public static readonly string ImagesAfterTransformCancelled = "Импортировано 0 изображений. Удалено с карты 0 файлов.";
		public static readonly string NoImagesDeleted = "Удалено с карты: 0 файлов.";
		public static readonly string FilesDeleted = "{0} с карты: {1} {2}.";
		public static readonly string ImagesDeletedLog = "{0} c карты: {1} {2}. Все файлы не удалялись из-за наличия других медиа файлов.";
		public static readonly string ErrorAtDeleting = "Не удалось очистить карту";
		public static readonly string NoRestartWarning = "Внимание! Не перезапускайте импорт - все поддерживаемые изображения уже скопированы на компьютер.\nОтформатируйте карту вручную.";
		public static readonly string OversizedImages = "Некоторые изображения превышают {0} килобайт. Проверьте следующие папки:\n{1}";
		public static readonly string InformDeveloper = "Пожалуйста сообщите разработчику.";
		public static readonly string ImportSucceed = "Импорт завершен";
		public static readonly string ChooseTargetFolder = "Укажите путь к папке Aluminium products";
		public static readonly string StartLogInfo = "==================={0:F}===========";
		public static readonly string RestartLogInfo = "=========RESTART==={0:F}===========";
		public static readonly string InvalidState = "Illegal state";

		public static string ImagesRu(int count) {
			var forms = new string[] { "изображение", "изображения", "изображений" };
			return NumEnding(count, forms);
		}

		public static string ImagesRuNone(int count) {
			var forms = new string[] { "изображения", "изображений", "изображений" };
			return NumEnding(count, forms);
		}

		//[1,4,5] [яблоко, яблока, яблок]
		public static string NumEnding(int count, string[] forms) {
			int big = count % 100;
			if (11 <= big && big <= 19) {
				return forms[2];
			} else {
				int small = big % 10;
				switch (small) {
					case 1:
						return forms[0];
					case 2:
					case 3:
					case 4:
						return forms[1];
					default:
						//0,5,6,7,8,9
						return forms[2];
				}
			}
		}

		public static string DeletedMaleRu(int count) {
			var forms = new string[] { "удален", "удалено", "удалено" };
			return NumEnding(count, forms);
		}

		public static string Files(int count) {
			var forms = new string[] { "файл", "файла", "файлов" };
			return NumEnding(count, forms);
		}

		//максимальный размер файла в КБ
		public static string MaxSizeToString() {
			return (AppConstants.MaxSize / 1000).ToString();
		}
	}
}
