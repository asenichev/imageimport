﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace ImageImport {

	public class AppConstants {

		public static string ApplicationName {
			get {
#if DEBUG
				return "Импорт фотографий - отладка";
#else
				return "Импорт фотографий";
#endif
			}
		}

		//папка куда пишется лог приложения
		public static readonly string LogFolder = Path.Combine(
			Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
			"Логи", ApplicationName);

		//расширения файлов, поддерживаемые приложением
		public static readonly Regex SupportedExtension = new Regex(@"^.*\.jpg",RegexOptions.IgnoreCase);
		public static readonly string SupportedExtensionName = "jpg";
		
		//для использования в переименовынных файлах
		public static readonly string JpgExtension = ".JPG";

		//расширения файлов изображений и видео, поддерживаемые Windows
		public static readonly Regex MediaFiles = new Regex(@"^.*\.(jpeg|ico|png|bmp|tif|tiff|gif|hdp|wdp|raw|wm|wmv|asf|m2ts|m2t|mov|qt|avi|wtv|dvr-ms|mp4|mov|m4v|mpeg|mpg|mpe|m1v|mp2|mpv2|mod|vob|m1v)$", RegexOptions.IgnoreCase);

		public static readonly string Unknown = "Unknown";

		//максимальный размер изображения в байтах
		public static readonly int MaxSize = 200000;

	}
}
