﻿using System;
using System.IO;
using ImageImport;
using ImageImport.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {

	/*
	 * Тесты не покрывают ситуацию с невставленным съемным диском, это следует
	 * протестировать вручную на работающем приложении
	 */

	[TestClass]
	public class SourceDiskTests {

		[TestMethod]
		public void TestIsDiskIdentified() {
			var source = new SourceDisk("C");
			Assert.IsTrue(source.IsDiskIdentified);
		}

		[TestMethod]
		public void TestIsCardInserted() {
			var source = new SourceDisk("C");
			Assert.IsTrue(source.IsCardInserted);
		}

		[TestMethod]
		public void TestDiskName() {
			var source = new SourceDisk("C");
			Assert.AreEqual<string>(@"C:\", source.DiskName);
		}

		[TestMethod]
		public void TestMainFolder() {
			var source = new SourceDisk("C");
			Assert.AreEqual<string>(new DirectoryInfo(@"C:\").FullName, source.MainFolder.FullName);
		}

		[TestMethod]
		public void TestErrorMessage() {
			var source = new SourceDisk("C");
			Assert.AreEqual<string>(string.Empty, source.ErrorMessage);
		}

		[TestMethod]
		public void TestNullValues() {
			var source = new SourceDisk("Abrakadabra");
			Assert.IsFalse(source.IsDiskIdentified);
			Assert.IsNull(source.MainFolder);
			Assert.AreEqual<string>(string.Empty, source.DiskName);
			Assert.IsFalse(source.IsCardInserted);
			Assert.AreEqual<string>(string.Format(AppMessages.CardNotIdentified, "Abrakadabra"), source.ErrorMessage);
		}
	}
}
