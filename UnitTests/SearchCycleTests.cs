﻿using System;
using System.IO;
using ImageImport.Cycles;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;

namespace UnitTests {

	[TestClass]
	public class SearchCycleTests {

		[TestMethod]
		public void TestFilesAndFoldersCount() {
			var dir=new DirectoryInfo("../../TestFiles/SearchTests");
			var searchCycle = new SearchCycle(dir);
			Assert.AreEqual<int>(12, searchCycle.AllFilesCount);
		}

		[TestMethod]
		public void TestImageFilesCount() {
			var dir = new DirectoryInfo("../../TestFiles/SearchTests");
			var searchCycle = new SearchCycle(dir);
			Assert.AreEqual<int>(2, searchCycle.ImageFilesCount);
		}

		[TestMethod]
		public void TestImageFiles() {
			var dir = new DirectoryInfo("../../TestFiles/SearchTests");
			var searchCycle = new SearchCycle(dir);
			Assert.IsTrue(searchCycle.ImageFiles.Select(f=>f.Name).SequenceEqual<string>(ImageFiles()));
		}

		private IEnumerable<string> ImageFiles() {
			yield return "Photo1.JPG";
			yield return "Photo2.JPG";
		}

		[TestMethod]
		public void TestHasUnsupportedMedia() {
			var dir = new DirectoryInfo("../../TestFiles/SearchTests");
			var searchCycle = new SearchCycle(dir);
			Assert.IsTrue(searchCycle.HasUnsupportedMedia);
		}

		[TestMethod]
		public void TestUnsupportedMedia() {
			var dir = new DirectoryInfo("../../TestFiles/SearchTests");
			var searchCycle = new SearchCycle(dir);
			Assert.AreEqual<string>(UnsupportedMedia(),searchCycle.UnsupportedMediaList);
		}

		private string UnsupportedMedia() {
			return "D:\\Projects\\ImageImport\\UnitTests\\TestFiles\\SearchTests\\Media\\IcoFile.ico\r\nD:\\Projects\\ImageImport\\UnitTests\\TestFiles\\SearchTests\\Media\\PngFile.png\r\nD:\\Projects\\ImageImport\\UnitTests\\TestFiles\\SearchTests\\Media\\VidioFile.wmv";
		}

		[TestMethod]
		public void TestNoUnsupportedMedia() {
			var dir = new DirectoryInfo("../../TestFiles/SearchTests/EmptyFolder");
			var searchCycle = new SearchCycle(dir);
			Assert.IsFalse(searchCycle.HasUnsupportedMedia);
			Assert.AreEqual<string>(string.Empty, searchCycle.UnsupportedMediaList);
		}

		[TestMethod]
		public void TestUnsupportedMediaCount() {
			var dir = new DirectoryInfo("../../TestFiles/SearchTests");
			var searchCycle = new SearchCycle(dir);
			Assert.AreEqual<int>(3, searchCycle.MediaFilesCount);
		}

		[TestMethod]
		public void NoFiles(){
			var dir = new DirectoryInfo("../../TestFiles/SearchTests/EmptyFolder");
			var searchCycle = new SearchCycle(dir);
			Assert.AreEqual<int>(0, searchCycle.AllFilesCount);
			Assert.AreEqual<int>(0, searchCycle.ImageFilesCount);
			Assert.AreEqual<int>(0, searchCycle.ImageFiles.Count());
			Assert.AreEqual<int>(0, searchCycle.MediaFilesCount);
		}


	}
}
