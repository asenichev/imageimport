﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using ImageImport.Models;
using System.Linq;
using ImageImport.Cycles;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace UnitTests {

	[TestClass]
	public class RenameTests {

		IEnumerable<Photo> _photos;

		public RenameTests() {
			var files = GetFiles();
			_photos = files.Select(f => new Photo(f));
		}

		private IEnumerable<FileInfo> GetFiles() {
			yield return new FileInfo("../../TestFiles/RenameTests/Photo1.jpg");
			yield return new FileInfo("../../TestFiles/RenameTests/Photo2.jpg");
			yield return new FileInfo("../../TestFiles/RenameTests/Photo3.jpg");
		}

		[TestMethod]
		public void TestRenamedPhotos() {
			string guid = "584DA01F";
			var rename = new Rename(_photos, guid);
			Assert.IsTrue(Answer().SequenceEqual(rename.RenamedPhotos.Select(p => p.Name)));
		}

		private IEnumerable<string> Answer() {
			yield return "104600-584DA01F-0001-104600.JPG";
			yield return "104600-584DA01F-0002-104615.JPG";
			yield return "104600-584DA01F-0003-104642.JPG";
		}

		//jan 2013 - failed
		public async Task TestCollisions() {
			string folder = @"S:\Metals&Minerals\AL.PRODUCTS\2013\фото\01 Архив\Январь";
			var search = new SearchCycle(new DirectoryInfo(folder));
			var transform = new TransformCycle(search.ImageFiles, Progress, new CancellationTokenSource().Token);
			await transform.ReadMetadataAsync();
			//сгруппировать все снимки по дням
			var days = transform.Photos.GroupBy(p => p.DateString);
			//для каждого дня
			foreach (var group in days) {
				//минимальное время снимка
				var min=group.Min(p=>p.DateTimeTaken);
				//фотография с таким временем только одна
				Assert.IsTrue(group.Where(p => p.DateTimeTaken == min).Count() < 2);
			}
		}

		private void Progress(int obj) {
			//stub
		}

	}
}
