﻿using System;
using System.Diagnostics;
using System.IO;
using ImageImport.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {

	[TestClass]
	public class PhotoTests {

		[TestMethod]
		public void TestDateString() {
			var fileInfo = new FileInfo("../../TestFiles/PhotoTests/Photo1.jpg");
			var photo = new Photo(fileInfo);
			Assert.AreEqual<string>("2014-03-12", photo.DateString);
		}

		[TestMethod]
		public void TestDateString1() {
			var fileInfo = new FileInfo("../../TestFiles/PhotoTests/Photo2.jpg");
			var photo = new Photo(fileInfo);
			Assert.AreEqual<string>("2014-03-10", photo.DateString);
		}

		[TestMethod]
		public void TestDateString2() {
			var fileInfo = new FileInfo("../../TestFiles/PhotoTests/Photo3.jpg");
			var photo = new Photo(fileInfo);
			Assert.AreEqual<string>("2014-03-03", photo.DateString);
		}

		[TestMethod]
		public void TestDateTime() {
			var fileInfo = new FileInfo("../../TestFiles/PhotoTests/Photo1.jpg");
			var photo = new Photo(fileInfo);
			Assert.AreEqual<DateTime>(new DateTime(2014, 3, 12, 6, 17, 6), photo.DateTimeTaken);
		}
		[TestMethod]
		public void TestDateTime1() {
			var fileInfo = new FileInfo("../../TestFiles/PhotoTests/Photo2.jpg");
			var photo = new Photo(fileInfo);
			Assert.AreEqual<DateTime>(new DateTime(2014, 3, 10, 23, 41, 59), photo.DateTimeTaken);
		}
		[TestMethod]
		public void TestDateTime2() {
			var fileInfo = new FileInfo("../../TestFiles/PhotoTests/Photo3.jpg");
			var photo = new Photo(fileInfo);
			Assert.AreEqual<DateTime>(new DateTime(2014, 3, 3, 12, 6, 14), photo.DateTimeTaken);
		}

		[TestMethod]
		public void TestNotJPG() {
			var fileInfo = new FileInfo("../../TestFiles/PhotoTests/Wrong.jpg");
			var photo = new Photo(fileInfo);
			Assert.AreEqual<string>(Photo.DateNotRecognized, photo.DateString);
			Assert.AreEqual<DateTime>(DateTime.MinValue, photo.DateTimeTaken);
		}
	}
}
