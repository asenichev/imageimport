﻿using System;
using System.IO;
using ImageImport;
using ImageImport.Cycles;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {

	[TestClass]
	public class PreparationCycleTests {

		[TestMethod]
		public void TestIsReady() {
			var cycle = new PreparationCycle(@"C:\", @"C:\");
			Assert.IsTrue(cycle.IsReady);
		}

		[TestMethod]
		public void TestErrorMessage() {
			var cycle = new PreparationCycle(@"Abrakadabra", @"C:\");
			Assert.AreEqual<string>(string.Format(AppMessages.CardNotIdentified,
				"Abrakadabra"), cycle.ErrorMessage);
		}

		[TestMethod]
		public void TestErrorMessage1() {
			var cycle = new PreparationCycle(@"C:\", @"Abrakadabra");
			Assert.AreEqual<string>(@"Нет доступа к D:\Projects\ImageImport\UnitTests\bin\Debug\Abrakadabra", cycle.ErrorMessage);
		}

		[TestMethod]
		public void TestErrorMessage2() {
			var cycle = new PreparationCycle(@"C:\", @"C:\");
			Assert.AreEqual<string>(string.Empty, cycle.ErrorMessage);
		}

		[TestMethod]
		public void TestTargetFolder() {
			var cycle = new PreparationCycle(@"Abrakadabra", @"C:\");
			Assert.AreEqual<string>(new DirectoryInfo(@"C:\").FullName, cycle.TargetFolder.FullName);
		}

		[TestMethod]
		public void TestSourceFolder() {
			var cycle = new PreparationCycle(@"C:\", @"Abrakadabra");
			Assert.AreEqual<string>(new DirectoryInfo(@"C:\").FullName, cycle.SourceFolder.FullName);
		}

		[TestMethod]
		public void TestNullValues() {
			var cycle = new PreparationCycle(@"Abrakadabra", @"Abrakadabra");
			Assert.IsFalse(cycle.IsReady);
			Assert.IsNull(cycle.SourceFolder);
			Assert.IsNull(cycle.TargetFolder);
		}

		[TestMethod]
		public void TestNullValues1() {
			var cycle = new PreparationCycle(string.Empty, string.Empty);
			Assert.IsFalse(cycle.IsReady);
			Assert.IsNull(cycle.SourceFolder);
			Assert.IsNull(cycle.TargetFolder);
		}

		[TestMethod]
		public void TestNullValues2() {
			var cycle = new PreparationCycle(null, null);
			Assert.IsFalse(cycle.IsReady);
			Assert.IsNull(cycle.SourceFolder);
			Assert.IsNull(cycle.TargetFolder);
		}

	}
}
