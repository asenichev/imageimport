﻿using System;
using System.IO;
using ImageImport;
using ImageImport.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {

	[TestClass]
	public class TargetFolderTests {

		[TestMethod]
		public void TestDirectory() {
			var target = new TargetFolder(@"C:\");
			Assert.AreEqual<string>(new DirectoryInfo(@"C:\").FullName, target.Directory.FullName);
		}

		[TestMethod]
		public void IsAccessible() {
			var target = new TargetFolder(@"C:\");
			Assert.IsTrue(target.IsAccessible);
		}

		[TestMethod]
		public void IsNotSet() {
			var target = new TargetFolder(AppConstants.Unknown);
			Assert.IsTrue(target.IsNotSet);
		}

		[TestMethod]
		public void TestErrorMessage() {
			var target = new TargetFolder(@"C:\");
			Assert.AreEqual<string>(string.Empty, target.ErrorMessage);
		}

		[TestMethod]
		public void TestNullValues() {
			var target = new TargetFolder("Abrakadabra");
			Assert.IsNotNull(target.Directory);
			Assert.IsFalse(target.IsAccessible);
			Assert.IsFalse(target.IsNotSet);
			Assert.AreEqual<string>(AppMessages.FolderNotAccessible + target.Directory.FullName, target.ErrorMessage);
		}
	}
}
