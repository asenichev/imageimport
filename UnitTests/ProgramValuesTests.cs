﻿using System;
using System.Collections.Generic;
using ImageImport.MainWindows;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {

	[TestClass]
	public class ProgramValuesTests {
		/*
			* 
			* Цикл по 100 импортов упал на 500 000 попытке
			* Цикл 1 миллион по 100 прошел
			* 10 тысяч циклов по 100 импортов - это примерно 27 лет работы с
			* невероятной нагрузкой
			* 
			*/
		[TestMethod]
		public void TestImportId() {
			int imports = 10000;
			for (int i = 0; i < imports; i++)
				if (!NoCollision())
					Assert.Fail("Failed at attempt # " + i);
		}

		private bool NoCollision() {
			int count = 100;
			var set = new HashSet<string>();
			for (int i = 0; i < count; i++) {
				var pvt = new ProgramValues();
				set.Add(pvt.ImportId);
			}
			return set.Count == count;
		}
	}
}
