﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ImageImport.Cycles;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {

	[TestClass]
	public class DeleteCycleTests {

		static DirectoryInfo Folder = new DirectoryInfo("../../TestFiles/DeleteTests");

		CancellationTokenSource _tokenSource;

		void CleanFolder() {
			foreach (var fileSystemInfo in Folder.GetFileSystemInfos())
				DeleteFileSystemInfo(fileSystemInfo);
		}

		void DeleteFileSystemInfo(FileSystemInfo fileSystemInfo) {
			var directoryInfo = fileSystemInfo as DirectoryInfo;
			if (directoryInfo != null) {
				foreach (var childInfo in directoryInfo.GetFileSystemInfos()) {
					DeleteFileSystemInfo(childInfo);
				}
			}
			fileSystemInfo.Attributes = FileAttributes.Normal;
			fileSystemInfo.Delete();
		}

		void CreateStubFiles() {

			//некий рабочий невидимый файл в главной директории
			var unknownFile = new FileInfo(Path.Combine(Folder.FullName, "UnknownFile.unn"));
			using (unknownFile.Create()) { }
			unknownFile.Refresh();
			unknownFile.Attributes = FileAttributes.Hidden;

			//пустая папка
			Directory.CreateDirectory(Path.Combine(Folder.FullName, "EmptyDir"));

			//файлы изображений в подпапках
			var subFolder = Directory.CreateDirectory(Path.Combine(Folder.FullName, "DB", "Date1"));
			using (File.Create(Path.Combine(subFolder.FullName, "IMG000123.JPG"))) { }
			using (File.Create(Path.Combine(subFolder.FullName, "IMG000124.JPG"))) { }
			using (File.Create(Path.Combine(subFolder.FullName, "IMG000125.JPG"))) { }

			//файлы изображений в подпапках
			var subFolder2 = Directory.CreateDirectory(Path.Combine(Folder.FullName, "DB", "Date2"));
			using (File.Create(Path.Combine(subFolder2.FullName, "PDC000123.JPG"))) { }
			using (File.Create(Path.Combine(subFolder2.FullName, "PDC000124.JPG"))) { }
			using (File.Create(Path.Combine(subFolder2.FullName, "PDC000125.JPG"))) { }
		}

		void CreateStubMedia() {

			//медиа файлы изображений в подпапках
			var subFolder = Directory.CreateDirectory(Path.Combine(Folder.FullName, "MyIcons"));
			using (File.Create(Path.Combine(subFolder.FullName, "ok.ico"))) { }
			using (File.Create(Path.Combine(subFolder.FullName, "image.png"))) { }

			//медиа файлы видео в подпапках
			var subFolder2 = Directory.CreateDirectory(Path.Combine(Folder.FullName, "MyVideo"));
			using (File.Create(Path.Combine(subFolder2.FullName, "movie1.mov"))) { }
			using (File.Create(Path.Combine(subFolder2.FullName, "movie2.avi"))) { }
			using (File.Create(Path.Combine(subFolder2.FullName, "movie3.mp4"))) { }
		}

		//удалить все файлы
		[TestMethod]
		public async Task TestIsOK() {
			CleanFolder();
			CreateStubFiles();
			var cycle = DeleteCycle.DeleteAllCycle(Folder, StubProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.IsTrue(cycle.IsOK);
		}

		private void StubProgressReporter(int obj) {
			//nil
		}

		//удалить только изображения, когда есть медиафайлы
		[TestMethod]
		public async Task TestIsOK3() {
			CleanFolder();
			CreateStubFiles();
			CreateStubMedia();
			var cycle = DeleteCycle.DeleteImagesCycle(Folder, StubProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.IsTrue(cycle.IsOK);
		}

		//удалить все файлы
		[TestMethod]
		public async Task TestActuallyDeletes() {
			CleanFolder();
			CreateStubFiles();
			var cycle = DeleteCycle.DeleteAllCycle(Folder, StubProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.AreEqual<int>(0, Folder.GetFileSystemInfos("*", SearchOption.AllDirectories).Length);
		}

		//удалить только изображения, когда есть медиа файлы
		[TestMethod]
		public async Task TestActuallyDeletes2() {
			CleanFolder();
			CreateStubFiles();
			CreateStubMedia();
			var cycle = DeleteCycle.DeleteImagesCycle(Folder, StubProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.AreEqual<int>(12, Folder.GetFileSystemInfos("*", SearchOption.AllDirectories).Length);
		}

		//все файлы
		[TestMethod]
		public async Task TestIsCancelled() {
			CleanFolder();
			CreateStubFiles();
			_tokenSource = new CancellationTokenSource();
			var cycle = DeleteCycle.DeleteAllCycle(Folder, CancellationProgressReporter, _tokenSource.Token);
			await cycle.DeleteFilesAsync();
			Assert.IsTrue(cycle.IsCancelled);
			Assert.IsFalse(cycle.IsOK);
			Assert.AreEqual<int>(2, cycle.Count);
		}

		private void CancellationProgressReporter(int index) {
			if (index == 2)
				_tokenSource.Cancel();
		}

		//только изображения
		[TestMethod]
		public async Task TestIsCancelled2() {
			CleanFolder();
			CreateStubFiles();
			CreateStubMedia();
			_tokenSource = new CancellationTokenSource();
			var cycle = DeleteCycle.DeleteImagesCycle(Folder, CancellationProgressReporter, _tokenSource.Token);
			await cycle.DeleteFilesAsync();
			Assert.IsTrue(cycle.IsCancelled);
			Assert.IsFalse(cycle.IsOK);
			Assert.AreEqual<int>(2, cycle.Count);
		}

		//все файлы
		[TestMethod]
		public async Task TestCount() {
			CleanFolder();
			CreateStubFiles();
			var cycle = DeleteCycle.DeleteAllCycle(Folder, StubProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.AreEqual<int>(11, cycle.Count);
		}

		//только изображения
		[TestMethod]
		public async Task TestCount2() {
			CleanFolder();
			CreateStubFiles();
			CreateStubMedia();
			var cycle = DeleteCycle.DeleteImagesCycle(Folder, StubProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.AreEqual<int>(6, cycle.Count);
		}

		//пустая папка
		[TestMethod]
		public async Task TestCount3() {
			CleanFolder();
			var cycle = DeleteCycle.DeleteAllCycle(Folder, StubProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.AreEqual<int>(0, cycle.Count);
		}

		//все файлы
		[TestMethod]
		public async Task TestIsError() {
			CleanFolder();
			CreateStubFiles();
			var cycle = DeleteCycle.DeleteAllCycle(Folder, ErrorProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.IsTrue(cycle.IsError);
			Assert.IsFalse(cycle.IsOK);
			Assert.AreEqual<int>(2, cycle.Count);
		}

		private void ErrorProgressReporter(int index) {
			if (index == 2)
				throw new IOException("Test exception thrown in progress reporter");
		}

		//только изображения
		[TestMethod]
		public async Task TestIsError2() {
			CleanFolder();
			CreateStubFiles();
			CreateStubMedia();
			var cycle = DeleteCycle.DeleteImagesCycle(Folder, ErrorProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.IsTrue(cycle.IsError);
			Assert.IsFalse(cycle.IsOK);
			Assert.AreEqual<int>(2, cycle.Count);
		}

		//все файлы
		[TestMethod]
		public async Task TestErrorMessage() {
			CleanFolder();
			CreateStubFiles();
			var cycle = DeleteCycle.DeleteAllCycle(Folder, ErrorProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.AreEqual<string>("Test exception thrown in progress reporter", cycle.ErrorMessage);
		}

		//только изображения
		[TestMethod]
		public async Task TestErrorMessage2() {
			CleanFolder();
			CreateStubFiles();
			CreateStubMedia();
			var cycle = DeleteCycle.DeleteImagesCycle(Folder, ErrorProgressReporter, new CancellationTokenSource().Token);
			await cycle.DeleteFilesAsync();
			Assert.AreEqual<string>("Test exception thrown in progress reporter", cycle.ErrorMessage);
		}

		public void TestNullValues() {
			//нулевой токен компилятор не пропускает
			//отсутствие нулевого делегата прогресса приложение гарантирует
 			//делать защиту в классе DeleteCycle от некорректного пути SourceFolder
			//не имеет смысла, приложение гарантирует корректный путь.
		}
	}
}
