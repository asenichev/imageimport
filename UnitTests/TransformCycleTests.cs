﻿using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ImageImport.Cycles;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.TestFiles {

	[TestClass]
	public class TransformTests {

		static DirectoryInfo Folder = new DirectoryInfo("../../TestFiles/TransformTests");

		[TestMethod]
		public async Task TestTransformOK() {
			var files = Folder.EnumerateFiles();
			var token=new CancellationTokenSource().Token;
			var transform = new TransformCycle(files,StubProgress,token);
			await transform.ReadMetadataAsync();
			Assert.IsTrue(transform.IsOK);
		}

		private void StubProgress(int obj) {
			//nil
		}

		[TestMethod]
		public async Task TestTransformCancelled() {
			var files = Folder.EnumerateFiles();
			var tokenSource = new CancellationTokenSource();
			var transform = new TransformCycle(files, StubProgress, tokenSource.Token);
			tokenSource.Cancel();
			await transform.ReadMetadataAsync();
			Assert.IsTrue(transform.IsCancelled);
		}

		[TestMethod]
		public async Task TestPhotos() {
			var files = Folder.EnumerateFiles();
			var token = new CancellationTokenSource().Token;
			var transform = new TransformCycle(files, StubProgress, token);
			await transform.ReadMetadataAsync();
			Assert.IsTrue(transform.Photos.Select(p=>p.FullName).SequenceEqual(Photos()));
		}

		private System.Collections.Generic.IEnumerable<string> Photos() {
			yield return "D:\\Projects\\ImageImport\\UnitTests\\TestFiles\\TransformTests\\Photo1.JPG";
			yield return "D:\\Projects\\ImageImport\\UnitTests\\TestFiles\\TransformTests\\Photo2.JPG";
			yield return "D:\\Projects\\ImageImport\\UnitTests\\TestFiles\\TransformTests\\Photo3.JPG";
		}

		[TestMethod]
		public async Task TestIsOK() {
			var files = Folder.EnumerateFiles();
			var token = new CancellationTokenSource().Token;
			var transform = new TransformCycle(files, StubProgress, token);
			await transform.ReadMetadataAsync();
			Assert.IsTrue(transform.IsOK);
		}

	}
}
