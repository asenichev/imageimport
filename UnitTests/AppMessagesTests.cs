﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {

	[TestClass]
	public class AppMessagesTests {

		[TestMethod]
		public void NumEndings() {
			string[] forms = new string[] { "яблоко", "яблока", "яблок" };
			Assert.AreEqual<string>("яблока", ImageImport.AppMessages.NumEnding(2, forms));
		}

		[TestMethod]
		public void NumEndings1() {
			string[] forms = new string[] { "яблоко", "яблока", "яблок" };
			Assert.AreEqual<string>("яблока", ImageImport.AppMessages.NumEnding(1322, forms));
		}

		[TestMethod]
		public void NumEndings2() {
			string[] forms = new string[] { "яблоко", "яблока", "яблок" };
			Assert.AreEqual<string>("яблоко", ImageImport.AppMessages.NumEnding(1321, forms));
		}

		[TestMethod]
		public void NumEndings3() {
			string[] forms = new string[] { "яблоко", "яблока", "яблок" };
			Assert.AreEqual<string>("яблок", ImageImport.AppMessages.NumEnding(15, forms));
		}

		[TestMethod]
		public void NumEndings4() {
			string[] forms = new string[] { "яблоко", "яблока", "яблок" };
			Assert.AreEqual<string>("яблок", ImageImport.AppMessages.NumEnding(1558, forms));
		}
	}
}
