﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ImageImport.Cycles;
using ImageImport.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests {

	[TestClass]
	public class CopyCycleTests {

		static DirectoryInfo TargetFolder = new DirectoryInfo("../../TestFiles/CopyTests/TargetFolder");
		const string ImportId = "0A3F4D61";
		CancellationTokenSource _stubTokenSource;
		IEnumerable<Photo> _normalPhotos;
		IEnumerable<Photo> _normalAndOversizedPhotos;

		public CopyCycleTests() {
			_stubTokenSource = new CancellationTokenSource();
			_normalPhotos = Photos("../../TestFiles/CopyTests/SourceFolder");
			_normalAndOversizedPhotos = Photos("../../TestFiles/CopyTests/SourceFolderWithOVersized");
		}

		private IEnumerable<Photo> Photos(string folder) {
			var files = new DirectoryInfo(folder).EnumerateFiles();
			var transform = new TransformCycle(files, StubProgress, _stubTokenSource.Token);
			transform.ConvertImagesToPhotos();
			return transform.Photos;
		}

		void StubProgress(int value) {
			//nil
		}

		void CleanTargetFolder() {
			foreach (var fileSystemInfo in TargetFolder.GetFileSystemInfos())
				DeleteFileSystemInfo(fileSystemInfo);
		}

		private void DeleteFileSystemInfo(FileSystemInfo fileSystemInfo) {
			var directoryInfo = fileSystemInfo as DirectoryInfo;
			if (directoryInfo != null) {
				foreach (var childInfo in directoryInfo.GetFileSystemInfos()) {
					DeleteFileSystemInfo(childInfo);
				}
			}
			fileSystemInfo.Delete();
		}

		[TestMethod]
		public async Task TestCopyOK() {
			CleanTargetFolder();
			var copy = new CopyCycle(_normalPhotos, TargetFolder, ImportId, StubProgress, _stubTokenSource.Token);
			await copy.CopyImagesAsync();
			Assert.IsTrue(copy.IsOK);
		}

		[TestMethod]
		public async Task TestCopyCancelled() {
			CleanTargetFolder();
			var tokenSource = new CancellationTokenSource();
			var copy = new CopyCycle(_normalPhotos, TargetFolder, ImportId, StubProgress, tokenSource.Token);
			tokenSource.Cancel();
			await copy.CopyImagesAsync();
			Assert.IsTrue(copy.IsCancelled);
		}

		[TestMethod]
		public async Task TestCount() {
			CleanTargetFolder();
			var copy = new CopyCycle(_normalPhotos, TargetFolder, ImportId, StubProgress, _stubTokenSource.Token);
			await copy.CopyImagesAsync();
			Assert.AreEqual<int>(3, copy.Count);
		}

		[TestMethod]
		public async Task TestHasOversized() {
			CleanTargetFolder();
			var copy = new CopyCycle(_normalAndOversizedPhotos, TargetFolder, ImportId, StubProgress, _stubTokenSource.Token);
			await copy.CopyImagesAsync();
			Assert.IsTrue(copy.HasOversized);
		}

		[TestMethod]
		public async Task TestOversizedFolders() {
			CleanTargetFolder();
			var copy = new CopyCycle(_normalAndOversizedPhotos, TargetFolder, ImportId, StubProgress, _stubTokenSource.Token);
			await copy.CopyImagesAsync();
			Assert.AreEqual<string>(OversizedFolders(), copy.OversizedFolders);
		}

		private string OversizedFolders() {
			return "2011-08-04\r\n2012-06-12";
		}

		[TestMethod]
		public async Task TestReport() {
			CleanTargetFolder();
			var copy = new CopyCycle(_normalAndOversizedPhotos, TargetFolder, ImportId, StubProgress, _stubTokenSource.Token);
			await copy.CopyImagesAsync();
			Assert.AreEqual<string>(CopyReport(), copy.Report);
		}

		private string CopyReport() {
			return "В папку \"2011-08-04\" импортировано 1 изображение\nВ папку \"2012-06-12\" импортировано 1 изображение\nВ папку \"2014-03-31\" импортировано 2 изображения";
		}

	}
}
