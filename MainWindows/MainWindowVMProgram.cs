﻿using System;
using System.Collections.Generic;
using System.IO;
using ImageImport.MainWindows;
using ImageImport.Models;
using ImageImport.States;
using log4net;

namespace ImageImport.MainWindows {

	/*
	 * Часть вью-модели, которая отвечает за взаимодействие
	 * с состояниями программы.
	 */
	public partial class MainWindowVM {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		IProgress<int> _progress;
		IState _currentState;
		ProgramValues _programValues;

		void InitProgram() {
			_progress = new Progress<int>(value => ProgressValue = value);
			_programValues = new ProgramValues();
			//неопределенное состояние присваивается напрямую,
			//поэтому параметры окна оно не меняет
			_currentState = new Undefined();
		}

		public void Run() {
			SetState(new Prepare(this));
		}

		public void SetState(IState state) {
			_currentState = state;
			MainInstruction = state.MainInstruction;
			SupInstruction = state.SupInstruction;
			ActivePage = state.ActivePage;
			CanCancel = state.CanCancel;
			CanRestart = state.CanRestart;
			CanChangeFolders = state.CanChangeFolders;
			Result = state.Result;
			_currentState.Invoke();
		}

		void ExecuteRestart() {
			_programValues = new ProgramValues();
			log.Info(string.Format(AppMessages.RestartLogInfo, DateTime.Now));
			SetState(new Prepare(this));
		}

		void ExecuteCancel() {
			_currentState.Cancel();
		}

		//установить максимальное значение индикатора прогресса
		public void SetProgressMax(int imageFilesCount, int allFilesCount, int mediaFilesCount) {
			
			ProgressMax =

				//для цикла преобразования
				imageFilesCount +

				//для цикла копирования
				imageFilesCount +

				//для цикла удаления
				//...в случае если будут удаляться только файлы изображений - 
				//	могут оказаться папки, которые не будут удалены
				//	но, поскольку количество папок как правило невелико,
				//	это не создаст значительного скачка в индикаторе прогресса
				allFilesCount - mediaFilesCount;
		}

		public void ReportProgress(int value) {
			_progress.Report(value);
		}

		public void OnClosing(System.ComponentModel.CancelEventArgs e) {
			_currentState.OnClosing(e);
		}

		public string ImportId {
			get {
				return _programValues.ImportId;
			}
		}

		public DirectoryInfo SourceFolder {
			get {
				return _programValues.SourceFolder;
			}
			set {
				_programValues.SourceFolder = value;
			}
		}

		public DirectoryInfo TargetFolder {
			get {
				return _programValues.TargetFolder;
			}
			set {
				_programValues.TargetFolder = value;
			}
		}

		public int ImageFilesCount {
			get {
				return _programValues.FoundCount;
			}
			set {
				_programValues.FoundCount = value;
			}
		}

		public IEnumerable<FileInfo> ImageFiles {
			get {
				return _programValues.ImageFiles;
			}
			set {
				_programValues.ImageFiles = value;
			}
		}

		public IEnumerable<Photo> Photos {
			get {
				return _programValues.Photos;
			}
			set {
				_programValues.Photos = value;
			}
		}

		public bool HasUnsupportedMedia {
			get {
				return _programValues.HasUnsupportedMedia;
			}
			set {
				_programValues.HasUnsupportedMedia = value;
			}
		}

		public string UnsupportedMedia {
			get {
				return _programValues.UnsupportedMedia;
			}
			set {
				_programValues.UnsupportedMedia = value;
			}
		}

		public int CopiedCount {
			get {
				return _programValues.CopiedCount;
			}
			set {
				_programValues.CopiedCount = value;
			}
		}

		public bool HasOversizedImages {
			get {
				return _programValues.HasOversizedImages;
			}
			set {
				_programValues.HasOversizedImages = value;
			}
		}

		public string OversizedFolders {
			get {
				return _programValues.OversizedFolders;
			}
			set {
				_programValues.OversizedFolders = value;
			}
		}

		public string CopyReport {
			get {
				return _programValues.CopyReport;
			}
			set {
				_programValues.CopyReport = value;
			}
		}

		public int DeletedCount {
			get {
				return _programValues.DeletedCount;
			}
			set {
				_programValues.DeletedCount = value;
			}
		}

		public string ErrorAtDelete {
			get {
				return _programValues.ErrorAtDelete;
			}
			set {
				_programValues.ErrorAtDelete = value;
			}
		}
	}
}
