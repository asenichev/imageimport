﻿using System;
using System.Threading;
using System.Windows.Input;
using CookbookMVVM;
using CSUtil;
using ImageImport.Cycles;
using ImageImport.DiskInputWindows;

namespace ImageImport.MainWindows {

	public partial class MainWindowVM : ViewModelBase {

		string _mainInstruction;
		string _supInstruction;
		int _progressValue;
		int _progressMax;
		Page _activePage;
		Result _result;//от свойства Result зависит какую иконку покажет окно с результатом

		ICommand _cancel;
		ICommand _changeSource;
		ICommand _changeTarget;
		ICommand _restart;

		bool _canRestart;
		bool _canCancel;
		bool _canChangeFolders;

		public MainWindowVM() {
			//выставить нулевые инструкции и нулевой индикатор прогресса
			InitVisuals();
			//инициировать команды
			InitCommands();
			//инициировать часть, отвественную за цикл программы
			InitProgram();
		}

		private void InitVisuals() {
			_mainInstruction = string.Empty;
			_supInstruction = string.Empty;
			_activePage = Page.Progress;
			_progressValue = 0;
			_progressMax = 1;
		}

		private void InitCommands() {
			_cancel = new RelayCommand(ExecuteCancel);
			_changeSource = new ChangeSourceCommand();
			_changeTarget = new ChangeTargetCommand();
			_restart = new RelayCommand(ExecuteRestart);
			_canRestart = true;
			_canCancel = true;
			_canChangeFolders = true;
		}

		public bool CanCancel {
			get {
				return _canCancel;
			}
			set {
				if (_canCancel != value) {
					_canCancel = value;
					OnPropertyChanged("CanCancel");
				}
			}
		}

		public bool CanRestart {
			get {
				return _canRestart;
			}
			set {
				if (_canRestart != value) {
					_canRestart = value;
					OnPropertyChanged("CanRestart");
				}
			}
		}

		public Page ActivePage {
			get {
				return _activePage;
			}
			set {
				if (_activePage != value) {
					_activePage = value;
					OnPropertyChanged("ActivePage");
				}
			}
		}

		public string MainInstruction {
			get {
				return _mainInstruction;
			}
			set {
				if (_mainInstruction != value) {
					_mainInstruction = value;
					OnPropertyChanged("MainInstruction");
				}
			}
		}

		public string SupInstruction {
			get {
				return _supInstruction;
			}
			set {
				if (_mainInstruction != value) {
					_supInstruction = value;
					OnPropertyChanged("SupInstruction");
				}
			}
		}

		public int ProgressMax {
			get {
				return _progressMax;
			}
			set {
				_progressMax = value;
				OnPropertyChanged("ProgressMax");
			}
		}

		public int ProgressValue {
			get {
				return _progressValue;
			}
			set {
				_progressValue = value;
				OnPropertyChanged("ProgressValue");
			}
		}


		public Result Result {
			get {
				return _result;
			}
			set {
				if (!_restart.Equals(value)) {
					_result = value;
					OnPropertyChanged("Result");
				}
			}
		}

		public ICommand Cancel {
			get {
				return _cancel;
			}
		}

		public ICommand ChangeSource {
			get {
				return _changeSource;
			}
		}

		public ICommand ChangeTarget {
			get {
				return _changeTarget;
			}
		}

		public bool CanChangeFolders {
			get {
				return _canChangeFolders;
			}
			set {
				if (_canChangeFolders != value) {
					_canChangeFolders = value;
					OnPropertyChanged("CanChangeFolders");
				}
			}
		}

		public ICommand Restart {
			get {
				return _restart;
			}
		}

	}
}
