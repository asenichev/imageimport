﻿using System;
using System.Collections.Generic;
using System.IO;
using ImageImport.Models;

namespace ImageImport.MainWindows {

	/*
	 * Содержит параметры приложения, которые заполняются по мере выполнения
	 * программы
	 */

	public class ProgramValues {

		//папка-источник
		DirectoryInfo _sourceFolder;

		//папка-цель
		DirectoryInfo _targetFolder;

		//количество найденных изображений
		int _foundCount;

		//набор данных файлов изображений
		IEnumerable<FileInfo> _imageFiles;

		//набор метаданных изображений
		IEnumerable<Photo> _photos;

		//флаг наличия изображений, которые превышают допустимый размер
		bool _hasOversizedImages;

		//список папок в которых есть слишком большие изображения (одной строкой)
		string _oversizedFolders;

		//флаг наличия в папке-источнике медиа файлов, не поддерживаемых программой
		bool _hasUnsupportedMedia;

		//список неподдерживаемых медиа файлов в папке-источнике (одной строкой)
		string _unsupportedMediaMessage;

		//отчет о том, сколько файлов и куда скопированы программой (одной строкой)
		string _copyReport;

		//количество файлов, скопированных программой
		int _copiedCount;

		//количество файлов, удаленных программой
		int _deletedCount;

		//сообщение об ошибке, которая могла возникнуть во время удаления
		//...введен из-за того, что карта может быть заблокирована
		string _errorAtDelete;

		//Guid, идентифицирующий запуск программы
		Guid _importGuid;

		public ProgramValues() {
			_importGuid = Guid.NewGuid();
		}

		public string ImportId {
			get {
				return _importGuid.ToString().Substring(0, 8);
			}
		}

		public DirectoryInfo SourceFolder {
			get {
				return _sourceFolder;
			}
			set {
				_sourceFolder = value;
			}
		}

		public DirectoryInfo TargetFolder {
			get {
				return _targetFolder;
			}
			set {
				_targetFolder = value;
			}
		}

		public int FoundCount {
			get {
				return _foundCount;
			}
			set {
				_foundCount = value;
			}
		}

		public IEnumerable<FileInfo> ImageFiles {
			get {
				return _imageFiles;
			}
			set {
				_imageFiles = value;
			}
		}

		public IEnumerable<Photo> Photos {
			get {
				return _photos;
			}
			set {
				_photos = value;
			}
		}

		public bool HasUnsupportedMedia {
			get {
				return _hasUnsupportedMedia;
			}
			set {
				_hasUnsupportedMedia = value;
			}
		}

		public string UnsupportedMedia {
			get {
				return _unsupportedMediaMessage;
			}
			set {
				_unsupportedMediaMessage = value;
			}
		}

		public int CopiedCount {
			get {
				return _copiedCount;
			}
			set {
				_copiedCount = value;
			}
		}

		public bool HasOversizedImages {
			get {
				return _hasOversizedImages;
			}
			set {
				_hasOversizedImages = value;
			}
		}

		public string OversizedFolders {
			get {
				return _oversizedFolders;
			}
			set {
				_oversizedFolders = value;
			}
		}

		public string CopyReport {
			get {
				return _copyReport;
			}
			set {
				_copyReport = value;
			}
		}

		public int DeletedCount {
			get {
				return _deletedCount;
			}
			set {
				_deletedCount = value;
			}
		}

		public string ErrorAtDelete {
			get {
				return _errorAtDelete;
			}
			set {
				_errorAtDelete = value;
			}
		}
	}
}
