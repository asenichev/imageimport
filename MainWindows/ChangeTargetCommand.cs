﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookbookMVVM;

namespace ImageImport.MainWindows {

	class ChangeTargetCommand : RelayCommand {

		public ChangeTargetCommand() : base(ExecuteChangeTarget) { }

		static void ExecuteChangeTarget() {
			var newTarget = ChangeTargetDialogResult();
			if (newTarget != string.Empty) {
				Properties.Settings.Default.TargetFolder = newTarget;
				Properties.Settings.Default.Save();
			}
		}

		static string ChangeTargetDialogResult() {
			var dialog = new System.Windows.Forms.FolderBrowserDialog();
			dialog.Description = AppMessages.ChooseTargetFolder;
			dialog.RootFolder = Environment.SpecialFolder.MyComputer;
			if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				return dialog.SelectedPath;
			return string.Empty;
		}
	}
}
