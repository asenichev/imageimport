﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageImport.MainWindows {

	public partial class MainWindow : Window {

		//разрешает/не разрешает контроль закрытия
		bool _closingControlledByProgram;

		public MainWindow() {
			InitializeComponent();
		}

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e) {
			base.OnClosing(e);
			if (_closingControlledByProgram)
				((MainWindowVM)DataContext).OnClosing(e);
		}

		private void Close_Click(object sender, RoutedEventArgs e) {
			Close();
		}

		public void SetWindowCloseControlledByProgram(bool value) {
			_closingControlledByProgram = value;
		}
	}
}
