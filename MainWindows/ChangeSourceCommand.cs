﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CookbookMVVM;
using ImageImport.DiskInputWindows;

namespace ImageImport.MainWindows {

	class ChangeSourceCommand : RelayCommand {

		public ChangeSourceCommand() : base(ExecuteChangeSource) { }

		private static void ExecuteChangeSource() {
			var dialog = new DiskInputWindowVM();
			if (dialog.DialogResult == true) {
				Properties.Settings.Default.DiskName = dialog.DiskName;
				Properties.Settings.Default.Save();
			}
		}
	}
}
