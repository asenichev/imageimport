﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CookbookMVVM;
using CSUtil;
using ImageImport.Models;

namespace ImageImport.DiskInputWindows {

	class DiskInputWindowVM : ViewModelBase {

		static readonly string NotSelected = "(не выбран)";

		IEnumerable<string> _diskNames;
		string _diskName;
		ICommand _set;
		DiskInputWindow _view;

		public DiskInputWindowVM() {
			_diskNames = CreateDiskNames();
			_diskName = CurrentDiskName();
			_set = new RelayCommand(ExecuteSet,CanSet);
			_view = new DiskInputWindow();
			_view.DataContext = this;
			_view.ShowDialog();
		}


		private IEnumerable<string> CreateDiskNames() {
			var result=new List<string>();
			foreach (var driveInfo in DriveInfo.GetDrives()) {
				if (driveInfo.DriveType == DriveType.Removable) {
					result.Add(driveInfo.Name);
				}
			}
			result.Insert(0, NotSelected);
			return result;
		}

		private string CurrentDiskName() {
			 var sourceDisk = new SourceDisk(Properties.Settings.Default.DiskName);
			 if (sourceDisk.IsDiskIdentified)
				 return sourceDisk.DiskName;
			 else
				 return NotSelected;
		}

		private void ExecuteSet() {
			_view.DialogResult = true;
		}

		private bool CanSet() {
			return _diskName != NotSelected;
		}

		public IEnumerable<string> DiskNames {
			get {
				return _diskNames;
			}
		}

		public string DiskName {
			get {
				return _diskName;
			}
			set {
				_diskName = value;
			}
		}

		public ICommand Set {
			get {
				return _set;
			}
		}

		public bool? DialogResult {
			get {
				return _view.DialogResult;
			}
		}


	}
}
