﻿using System;
using System.IO;
using System.Windows;
using ImageImport.MainWindows;
using log4net;
using WpfDialogs;

namespace ImageImport {

	public partial class App : Application {

		private static readonly log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		MainWindow _view;
		MainWindowVM _viewModel;

		protected override void OnStartup(StartupEventArgs e) {
			base.OnStartup(e);
			ArrangeLogger();
			DispatcherUnhandledException += App_DispatcherUnhandledException;
			//вид
			_view = new MainWindow();
			//начальные значения контекста выставлены на окно прогресса с нулевыми величинами
			_viewModel = new MainWindowVM();
			//подключить контекст к виду
			_view.DataContext = _viewModel;
			//передать контроль за закрытием окна программе
			_view.SetWindowCloseControlledByProgram(true);
			//показать пустой вид
			_view.Show();
			//запустить программу
			_viewModel.Run();
		}

		//логгер
		void ArrangeLogger() {
			if (!Directory.Exists(AppConstants.LogFolder))
				Directory.CreateDirectory(AppConstants.LogFolder);
			Logging.Logger.Setup(AppConstants.LogFolder);
			log.Info(string.Format(AppMessages.StartLogInfo, DateTime.Now));
		}

		/*
		 * В случае непредвиденной ошибки появляется сообщение с деталями ошибки.
		 * После закрытия сообщения программа не прерывается, остается висеть
		 */
		void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e) {
			log.Error(e.Exception);
			ErrorMessage.Show(AppConstants.ApplicationName,
				AppMessages.UnexpectedError,
				supplementaryInstruction:
				string.Format(AppMessages.GeneralErrorSupInstruction, e.Exception.Message) +
				string.Format(AppMessages.CopiedDeleted, _viewModel.CopiedCount, _viewModel.DeletedCount));
			_view.SetWindowCloseControlledByProgram(false);
			e.Handled = true;
		}
	}
}
